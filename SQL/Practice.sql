USE [TRAINING];

GO
CREATE TABLE Employees
(
	Name nvarchar(100) NOT NULL,
	Department nvarchar(200) NOT NULL,
	Position nvarchar(200) NOT NULL,
	Country nvarchar(200) NOT NULL,
	Project nvarchar(300) NOT NULL
)

INSERT INTO Employees (Name, Department, Position, Country, Project) 
VALUES ('Alexander', 'ASP.NET Department', 'Programmer', 'Belarus', 'Luxedecor, TRSRS, Food Bank')

INSERT INTO Employees (Name, Department, Position, Country, Project) 
VALUES ('Dmitry', 'ASP.NET Department', 'Programmer', 'Belarus', 'Luxedecor')

INSERT INTO Employees (Name, Department, Position, Country, Project) 
VALUES ('Kevin', 'Sale Department, ASP.NET Department', 'Manager', 'USA', 'Luxedecor')

INSERT INTO Employees (Name, Department, Position, Country, Project) 
VALUES ('Eugen', 'ASP.NET Department', 'Head', 'Belarus', 'Why Go?, QB, Luxedecor')

INSERT INTO Employees (Name, Department, Position, Country, Project) 
VALUES ('Gleb', 'Node.js Department', 'Programmer', 'Ukrain', 'PLA')

INSERT INTO Employees (Name, Department, Position, Country, Project) 
VALUES ('Anastasiya', 'Design Department', 'Designer', 'Belarus', 'Luxedecor, Food Bank, Why Go?, QB, PLA')

INSERT INTO Employees (Name, Department, Position, Country, Project) 
VALUES ('Sergey', '* Departments', 'Head', 'Belarus', '*')

