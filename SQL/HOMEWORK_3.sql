USE Blog
GO

--IF OBJECT_ID('dbo.generateString') IS NOT NULL
--	DROP FUNCTION generateString
--GO

IF OBJECT_ID('dbo.rndView') IS NOT NULL
	DROP VIEW rndView
GO

CREATE VIEW rndView
AS
SELECT RAND() rndResult
GO

--CREATE FUNCTION generateString()
--RETURNS VARCHAR(160)
--AS
--BEGIN
--	DECLARE @name VARCHAR(160)
--	DECLARE @length INT
--	DECLARE @rndValue DECIMAL(18,18)
--	SET @rndValue = (SELECT rndResult
--						FROM rndView)
--	SET @name = ''
--	SET @length = CAST(@rndValue * 160 as INT)
--	WHILE @length <> 0
--		BEGIN
--		SET @rndValue = (SELECT rndResult
--						FROM rndView)
--		SELECT @name = @name + CHAR(CAST(@rndValue * 96 + 32 as INT))
--		SET @length = @length - 1
--		END
--	RETURN @name
--END
--GO

IF OBJECT_ID('dbo.TestUsers') IS NOT NULL
	DROP TABLE TestUsers
GO

IF OBJECT_ID('dbo.UserGroups') IS NOT NULL
	DROP TABLE UserGroups
GO

IF OBJECT_ID('dbo.TypesOfGroup') IS NOT NULL
	DROP TABLE TypesOfGroup
GO

CREATE TABLE TypesOfGroup
(
	Id int NOT NULL PRIMARY KEY IDENTITY(1, 1),
	Name VARCHAR(160) NOT NULL,
	Field1 VARCHAR(160) NOT NULL DEFAULT(dbo.generateString()),
	Field2 VARCHAR(160) NOT NULL DEFAULT(dbo.generateString()),
	Field3 VARCHAR(160) NOT NULL DEFAULT(dbo.generateString()),
	Field4 VARCHAR(160) NOT NULL DEFAULT(dbo.generateString()),
)
GO

CREATE TABLE UserGroups
(
	Id int NOT NULL PRIMARY KEY IDENTITY(1, 1),
	Name VARCHAR(160) NOT NULL,
	Field1 VARCHAR(160) NOT NULL DEFAULT(dbo.generateString()),
	Field2 VARCHAR(160) NOT NULL DEFAULT(dbo.generateString()),
	Field3 VARCHAR(160) NOT NULL DEFAULT(dbo.generateString()),
	Field4 VARCHAR(160) NOT NULL DEFAULT(dbo.generateString()),
	TypeOfGroupId int FOREIGN KEY REFERENCES TypesOfGroup(Id)
)
GO

CREATE TABLE TestUsers
(
	Id int NOT NULL PRIMARY KEY IDENTITY(1, 1),
	Username VARCHAR(160)  NOT NULL,
	UserPassword VARCHAR(160)  NOT NULL,
	Age INT NOT NULL,
	Country VARCHAR(160)  NOT NULL,
	Email VARCHAR(160)  NOT NULL,
	FName VARCHAR(160)  NOT NULL,
	LName VARCHAR(160)  NOT NULL,
	Birthdate DATE  NOT NULL,
	Genger BIT  NOT NULL,
	ZipCode INT   NULL,
	UserGroupId int FOREIGN KEY REFERENCES UserGroups(Id)
)
GO

DECLARE @RowCountTypesGroup int
DECLARE @RowCountUserGroups int
DECLARE @RowCountTypeUsers int
DECLARE @RowCount INT

SET @RowCountTypesGroup = 3
SET @RowCountUserGroups = 10
SET @RowCountTypeUsers = 300000
--------------------------------------------------

SET @RowCount = 0
WHILE @RowCount < @RowCountTypesGroup
BEGIN
	INSERT INTO TypesOfGroup
		(Name)
	VALUES
		('TypesOfGroup' + CAST(@RowCount AS varchar))

	SET @RowCount = @RowCount + 1
END
--------------------------------------------------

SET @RowCount = 0
WHILE @RowCount < @RowCountUserGroups
BEGIN
	INSERT INTO UserGroups
		(Name, TypeOfGroupId)
	VALUES
		('UserGroups' + CAST(@RowCount AS varchar), CAST((RAND()*(@RowCountTypesGroup - 1) + 1) AS int))

	SET @RowCount = @RowCount + 1
END
--------------------------------------------------
SET @RowCount = 0
WHILE @RowCount < @RowCountTypeUsers
BEGIN
	INSERT INTO TestUsers
		(Username
		,UserPassword
		,Age
		,Country
		,Email
		,FName
		,LName
		,Birthdate
		,Genger
		,ZipCode
		,UserGroupId)
	VALUES
		('Username' + CAST(@RowCount AS varchar)
		, 'UserPassword' + CAST(@RowCount AS varchar)
		,CAST(RAND() * 100 as INT)
		,'Country' + CAST(@RowCount AS varchar)
		,'Email' + CAST(@RowCount AS varchar)
		,'FName' + CAST(@RowCount AS varchar)
		,'LName' + CAST(@RowCount AS varchar)
		,GETDATE() + (365 * 2 * RAND() - 365)
		,CAST(ROUND(RAND(),0) AS BIT)
		,CAST(RAND() * 507 + 343 as INT)
		,CAST((RAND()*(@RowCountUserGroups - 1) + 1) AS int))

	SET @RowCount = @RowCount + 1
END
--------------------------------------------------

--SELECT TG.Id
--, TG.Name
--, COUNT(TU.Id) AS [USERS COUNT]
--, COUNT(UG.Id) AS [GROUPS COUNT]
--FROM TestUsers TU
--INNER JOIN UserGroups UG ON UG.Id = TU.UserGroupId
--INNER JOIN TypesOfGroup TG ON TG.Id = UG.TypeOfGroupId
--GROUP BY TG.Id, TG.Name
--ORDER BY [USERS COUNT]

--SELECT TU.Id
--, TU.Email 
--, tu.Username
--, UG.Id AS UG_ID
--, UG.Name AS UG_NAME
--FROM TestUsers TU
--INNER JOIN UserGroups UG ON UG.Id = TU.UserGroupId
--ORDER BY UG.Name
--GO

--SELECT TU.Id
--, UG.Id AS UG_ID
--, UG.Name AS UG_NAME
--FROM TestUsers TU
--INNER JOIN UserGroups UG ON UG.Id = TU.UserGroupId
--ORDER BY UG.Id, TU.Id
--GO

--SELECT*
--FROM TestUsers TU
--WHERE TU.Id = 350
--GO


--IF EXISTS (SELECT name FROM sys.indexes  
--            WHERE name = N'IX_TestUsers_ZipCode')  
--    DROP INDEX IX_TestUsers_ZipCode ON TESTUSERS;

--SELECT*
--FROM TestUsers TU
--WHERE TU.ZipCode = 848
--GO
   
--GO  
--CREATE INDEX IX_TestUsers_ZipCode   
--    ON TESTUSERS (ZipCode);
	
--SELECT*
--FROM TestUsers TU
--WHERE TU.ZipCode = 848
--GO	  

IF EXISTS (SELECT name FROM sys.indexes  
            WHERE name = N'IX_TU_AGE')  
    DROP INDEX IX_TU_AGE ON TestUsers;  
GO  
CREATE UNIQUE INDEX IX_TU_AGE ON TestUsers(UserGroupId, Age, Email) 
GO
IF OBJECT_ID('TESTPROCEDURE')IS NOT NULL
    DROP PROCEDURE TESTPROCEDURE
GO

CREATE PROCEDURE TESTPROCEDURE
AS
SELECT UG.Id 
, UG.Name AS UG_NAME
, TU.Email AS TU_Email
, TU.Age AS Age
FROM TestUsers TU
INNER JOIN UserGroups UG ON UG.Id = TU.UserGroupId
WHERE TU.Age > 18
ORDER BY UG.Id
GO

EXEC TESTPROCEDURE
GO