USE NORTHWND
GO

IF OBJECT_ID (N'dbo.getEmpoloyeeFullName', N'FN') IS NOT NULL  
    DROP FUNCTION dbo.getEmpoloyeeFullName;  
	GO
CREATE FUNCTION dbo.getEmpoloyeeFullName
(
	@Id INT
)
RETURNS VARCHAR(MAX)
AS 
BEGIN
	DECLARE @ResultValue VARCHAR(MAX)
	SET @ResultValue = (SELECT E.FirstName + ' ' + E.LastName
			FROM Employees E
				WHERE E.EmployeeID = @Id) 
	RETURN @ResultValue
		
END
GO

IF OBJECT_ID (N'dbo.getFullCostOrderDetail', N'FN') IS NOT NULL  
    DROP FUNCTION dbo.getFullCostOrderDetail;  
	GO
CREATE FUNCTION dbo.getFullCostOrderDetail
(
	@PId INT,
	@OId INT
)
RETURNS FLOAT
AS 
BEGIN
	DECLARE @ResultValue FLOAT
	SET @ResultValue = (SELECT Ord.UnitPrice*(1-ORD.Discount)*Ord.Quantity
			FROM [Order Details] Ord
				WHERE Ord.ProductID = @PId AND Ord.OrderID = @OId) 
	RETURN @ResultValue
		
END
GO
-- For each customer select all his orders, including product info and full employee info
SELECT C.ContactName
, C.CompanyName
,P.ProductName
, Ord.Quantity
, O.OrderDate
, dbo.getEmpoloyeeFullName(E.EmployeeID) AS [EMPLOYEE FULL NAME]
FROM Customers C
JOIN Orders O ON O.CustomerID = C.CustomerID
JOIN [Order Details] Ord ON Ord.OrderID = O.OrderID
JOIN Products P ON Ord.ProductID = P.ProductID
JOIN Employees E ON O.EmployeeID = E.EmployeeID
ORDER BY C.ContactName
GO
---------------------------------------------------------------------
-- For each customer select Total of all his orders (do not forget about discount)

SELECT C.ContactName
, C.CompanyName
, SUM(dbo.getFullCostOrderDetail(Ord.ProductID, Ord.OrderID)) AS TOTAL 
FROM Customers C
JOIN Orders O ON C.CustomerID = O.CustomerID
JOIN [Order Details] Ord ON Ord.OrderID = O.OrderID
GROUP BY C.CompanyName, C.ContactName
ORDER BY C.ContactName
GO
---------------------------------------------------------------------
-- Create a report showing the contact name and phone numbers for all employees, customers, and suppliers.

SELECT C.ContactName AS [Contact Name]
, C.Phone AS [Phone Number]
FROM Customers C
UNION
SELECT E.FirstName + ' ' + E.LastName AS [Contact Name]
, E.HomePhone AS [Phone Number]
FROM Employees E
UNION
SELECT S.ContactName AS [Contact Name]
, S.Phone AS [Phone Number]
FROM Suppliers S
GO
---------------------------------------------------------------------
-- Select average total (do not forget about discount) for each customer if average total less then 10 or more then 25.

SELECT C.ContactName
, C.CompanyName
, AVG(Ord.Quantity*Ord.UnitPrice*(1-Ord.Discount)) AS [AVERAGE TOTAL] 
FROM Customers C
JOIN Orders O ON C.CustomerID = O.CustomerID
JOIN [Order Details] Ord ON Ord.OrderID = O.OrderID
GROUP BY C.CompanyName, C.ContactName
HAVING AVG(Ord.Quantity*Ord.UnitPrice*(1-Ord.Discount)) > 25 OR AVG(Ord.Quantity*Ord.UnitPrice*(1-Ord.Discount)) < 10 
ORDER BY C.ContactName
GO
---------------------------------------------------------------------
-- Find the Companies that placed orders in 1997

SELECT C.ContactName
, C.CompanyName
, O.OrderDate
FROM Customers C
JOIN Orders O ON O.CustomerID = C.CustomerID
WHERE O.OrderDate >= '1997-01-01' AND O.OrderDate < '1998-01-01'
ORDER BY C.ContactName
GO
---------------------------------------------------------------------
-- Find the name of the company that placed order 10290 WITHOUT JOIN statement. Try to use sub select.

SELECT C.CompanyName
FROM Customers C
WHERE C.CustomerID = (
SELECT O.CustomerID
FROM Orders O
WHERE O.OrderID = '10290'
)
GO
---------------------------------------------------------------------
-- Select all customer with orders. If customer hasn't order - set 'No Order' instead of NULL.

SELECT C.ContactName
, C.CompanyName
, CAST (O.OrderDate AS nvarchar(100))
FROM Customers C
LEFT JOIN Orders O ON O.CustomerID = C.CustomerID
WHERE O.OrderID IS NOT NULL
UNION
SELECT C.ContactName
, C.CompanyName
, 'No Order'
FROM Customers C
LEFT JOIN Orders O ON O.CustomerID = C.CustomerID
WHERE O.OrderID IS NULL
GO
---------------------------------------------------------------------
-- Select 3 customers with orders total greater then others

SELECT C.ContactName
, C.CompanyName
FROM Customers C
CROSS APPLY 
(
SELECT TOP 3 
O.CustomerID
, SUM(dbo.getFullCostOrderDetail(Ord.ProductID, Ord.OrderID)) AS TOTAL
FROM Orders O
INNER JOIN [Order Details] Ord ON Ord.OrderID = O.OrderID
GROUP BY O.CustomerID
ORDER BY TOTAL DESC
)Res
WHERE Res.CustomerID = C.CustomerID
GO
---------------------------------------------------------------------
-- Select Customers with total for each order beetween 5-10 positions in the result

SELECT *
FROM
(
SELECT C.ContactName
, C.CompanyName
, Ord.Quantity*Ord.UnitPrice*(1-Ord.Discount) AS [ORDER TOTAL]
, ROW_NUMBER() OVER (PARTITION BY C.ContactName ORDER BY O.OrderID) AS Number
FROM Customers C
INNER JOIN Orders O ON O.CustomerID = C.CustomerID
INNER JOIN [Order Details] Ord ON Ord.OrderID = O.OrderID 
) Res
WHERE Res.Number BETWEEN 5 AND 10
GO
---------------------------------------------------------------------
-- Create View shows top 5 (or less) best selling products for each year.

IF OBJECT_ID('Top5BestSellingProducts', 'V') IS NOT NULL
DROP VIEW Top5BestSellingProducts
GO
CREATE VIEW Top5BestSellingProducts
AS
SELECT *
FROM 
(
SELECT YEAR(O.OrderDate) AS [YEAR]
, P.ProductName
, COUNT(ORD.ProductID) AS [COUNT SALES]
, ROW_NUMBER() OVER (PARTITION BY YEAR(O.OrderDate) ORDER BY COUNT(ORD.ProductID) DESC) AS Number
FROM Orders O
INNER JOIN [Order Details] Ord ON Ord.OrderID = O.OrderID
INNER JOIN Products P ON Ord.ProductID = P.ProductID
GROUP BY YEAR(O.OrderDate), P.ProductName
) Res 
WHERE Res.Number <= 5
GO
---------------------------------------------------------------------
--Show 10 latest orders for each customer which CustomerID starts with 'B'

SELECT C.CustomerID 
,C.ContactName
, CO.OrderDate
FROM Customers C
CROSS APPLY
(
SELECT TOP 10
O.OrderDate
FROM Orders O
WHERE O.CustomerID = C.CustomerID
ORDER BY O.OrderDate DESC
)AS CO
WHERE C.CustomerID LIKE 'B%'
GO
---------------------------------------------------------------------
--Show minimum, maximum and average order cost for 10 customers with at least one order, but with minimum number of orders.

SELECT TOP 10
C.CustomerID
, C.ContactName
, COUNT(O.OrderID) AS [ORDERS COUNT]
, MIN(O.[TOTAL ORDER]) AS [MIN ORDER]
, MAX(O.[TOTAL ORDER]) AS [MAX ORDER]
, AVG(O.[TOTAL ORDER]) AS [AVG ORDERS]
FROM Customers C
INNER JOIN 
	(
		SELECT [Order].OrderID
		, [Order].CustomerID
		, SUM(dbo.getFullCostOrderDetail(Ord.ProductId, Ord.OrderId)) AS [TOTAL ORDER]
		FROM Orders [Order]
		INNER JOIN [Order Details] Ord ON Ord.OrderID = [Order].OrderID
		GROUP BY [Order].OrderID, [Order].CustomerID
	) O ON O.CustomerID = C.CustomerID
GROUP BY C.CustomerID, C.ContactName
ORDER BY COUNT(O.OrderID)

---------------------------------------------------------------------
--Find total amount of all orders for each customer, sort customers by the value from maximum amount to minimum and show customers from 40 to 50 position in the result. 

SELECT *
FROM 
(
SELECT  C.ContactName
, SUM(dbo.getFullCostOrderDetail(Ord.ProductID, Ord.OrderID)) AS [TOTAL AMOUNT]
, ROW_NUMBER() OVER(ORDER BY SUM(dbo.getFullCostOrderDetail(Ord.ProductID, Ord.OrderID)) DESC) AS Number
FROM Customers C
INNER JOIN Orders O ON O.CustomerID = C.CustomerID
INNER JOIN [Order Details] Ord ON Ord.OrderID = O.OrderID
GROUP BY C.ContactName
) AS Res
WHERE RES.Number BETWEEN 40 AND 50
---------------------------------------------------------------------
--Show bestsellers: top 10 catories and top 3 products in each category with the highest volume of sales per product and per category.

SELECT Res.CategoryName
, Res.CategoryTotal
, Res.ProductName
, Res.ProductTotal
FROM
(
SELECT C.CategoryName
, C.CategoryTotal
, P.ProductName
, SUM(dbo.getFullCostOrderDetail(Ord.ProductID, Ord.OrderID)) AS ProductTotal
, ROW_NUMBER() OVER(PARTITION BY C.CategoryID ORDER BY SUM(dbo.getFullCostOrderDetail(Ord.ProductID, Ord.OrderID))DESC) AS Number
FROM 
(
	SELECT TOP 10  Cat.CategoryID
	, Cat.CategoryName
	, SUM(dbo.getFullCostOrderDetail(Ord.ProductID, Ord.OrderID)) AS CategoryTotal 
	FROM Categories Cat
	INNER JOIN Products P ON P.CategoryID = Cat.CategoryID
	INNER JOIN [Order Details] Ord ON Ord.ProductID = P.ProductID
	GROUP BY Cat.CategoryName, Cat.CategoryID
	ORDER BY SUM(dbo.getFullCostOrderDetail(Ord.ProductID, Ord.OrderID)) DESC
) C
INNER JOIN Products P ON P.CategoryID = C.CategoryID
INNER JOIN [Order Details] Ord ON Ord.ProductID = P.ProductID
GROUP BY P.ProductName, P.ProductID, C.CategoryID, C.CategoryName, C.CategoryTotal
)Res
WHERE Res.Number <= 3
ORDER BY Res.CategoryTotal DESC
---------------------------------------------------------------------
--Show sales volume for each employee for the 1997 year month by month. Result should contain 108 records (9*12)

CREATE TABLE #TEMP_TABLE
(
	UserId INT NOT NULL,
	MonthNumber INT NOT NULL
)
DECLARE @NumberMonth INT, @Id INT, @Counter INT
SET @NumberMonth = 1
SET @Id = (SELECT TOP 1 E.EmployeeID FROM Employees E LEFT JOIN #TEMP_TABLE TT ON TT.UserId = E.EmployeeID WHERE TT.UserId IS NULL)
WHILE (SELECT COUNT(*) FROM #TEMP_TABLE) < 12*(SELECT COUNT(*) FROM Employees E)
BEGIN
	IF @NumberMonth = 13
		BEGIN
			SET @NumberMonth = 1
			SET @Id = (SELECT TOP 1 E.EmployeeID FROM Employees E LEFT JOIN #TEMP_TABLE TT ON TT.UserId = E.EmployeeID WHERE TT.UserId IS NULL)
		END
	INSERT INTO #TEMP_TABLE
	(UserId, MonthNumber)
	VALUES(@Id, @NumberMonth)
	SET @NumberMonth = @NumberMonth + 1
END
SELECT dbo.getEmpoloyeeFullName(TT.UserId) AS NAME
, TT.MonthNumber
, Res.TOTAL
FROM
(
SELECT E.EmployeeID
, MONTH(O.OrderDate) AS [MONTH]
, SUM(dbo.getFullCostOrderDetail(Ord.ProductID, Ord.OrderID)) AS TOTAL
FROM Employees E
INNER JOIN Orders O ON E.EmployeeID = O.EmployeeID AND O.OrderDate >= '1997-01-01' AND O.OrderDate < '1998-01-01'
INNER JOIN [Order Details] Ord ON Ord.OrderID = O.OrderID
GROUP BY MONTH(O.OrderDate), E.EmployeeID
)Res
RIGHT JOIN #TEMP_TABLE TT ON TT.UserId = Res.EmployeeID AND Res.[MONTH] = TT.MonthNumber
ORDER BY dbo.getEmpoloyeeFullName(TT.UserId), TT.MonthNumber
DROP TABLE #TEMP_TABLE
GO

---------------------------------------------------------------------
--For 100 latest orders show sales volume per customer and category. Besides that the result should contain sales volume per category for all customers and per customer for all categories as well as sales volume for all customers and categories. see example

IF OBJECT_ID (N'dbo.getOrderPerCategoryCost', N'FN') IS NOT NULL  
    DROP FUNCTION dbo.getOrderPerCategoryCost;  
	GO
CREATE FUNCTION dbo.getOrderPerCategoryCost
(
	@OId INT,
	@CId INT
)
RETURNS FLOAT
AS 
BEGIN
	DECLARE @ResultValue FLOAT
	SET @ResultValue = (SELECT SUM(dbo.getFullCostOrderDetail(OD.ProductID, OD.OrderID))
		FROM Orders O
		INNER JOIN [Order Details] OD ON OD.OrderID = O.OrderID
		INNER JOIN Products P ON P.ProductID = OD.ProductID
		INNER JOIN Categories C ON C.CategoryID = P.CategoryID
		WHERE O.OrderID = @OId AND C.CategoryID = @CId
		GROUP BY C.CategoryID
		) 
	RETURN @ResultValue
		
END
GO

SELECT O.OrderID 
, Cat.CategoryName AS Category
, dbo.getOrderPerCategoryCost(O.OrderID, Cat.CategoryID) AS [Category sales volume]
, Cat.TotalCategory AS [Sales volume for all customers]
, C.ContactName AS Customer
, O.OrderTotal AS [Customer sales volume]
, C.TotalCustomer AS [Sales volume for all categories]
FROM
(
	SELECT TOP 100 Orders.OrderID
	, Orders.CustomerID
	, SUM(dbo.getFullCostOrderDetail([Order Details].ProductID, [Order Details].OrderID)) AS OrderTotal
	FROM Orders
	INNER JOIN [Order Details] ON [Order Details].OrderID = Orders.OrderID
	GROUP BY Orders.OrderID, Orders.OrderDate, Orders.CustomerID
	ORDER BY Orders.OrderDate DESC
) O
INNER JOIN 
(
	SELECT Cu.CustomerID
	, Cu.ContactName
	, SUM(dbo.getFullCostOrderDetail(OrdD.ProductId, OrdD.OrderId)) AS TotalCustomer
	FROM Customers Cu
	INNER JOIN Orders Ord ON Ord.CustomerID = Cu.CustomerID
	INNER JOIN [Order Details] OrdD ON OrdD.OrderID = Ord.OrderID
	GROUP BY Cu.CustomerID, Cu.ContactName
) C ON C.CustomerID = O.CustomerID
INNER JOIN [Order Details] OD ON OD.OrderID = O.OrderID
INNER JOIN Products P ON P.ProductID = OD.ProductID
INNER JOIN 
(
	SELECT Categ.CategoryID
	, Categ.CategoryName
	, SUM(dbo.getFullCostOrderDetail(OrdD.ProductId, OrdD.OrderId)) AS TotalCategory
	FROM Categories Categ
	INNER JOIN Products Prod ON Prod.CategoryID = Categ.CategoryID
	INNER JOIN [Order Details] OrdD ON OrdD.ProductID = Prod.ProductID
	GROUP BY Categ.CategoryID, Categ.CategoryName
) Cat ON Cat.CategoryID = P.CategoryID
GROUP BY O.OrderID, O.OrderTotal, C.CustomerID, C.ContactName, C.TotalCustomer, Cat.CategoryID, Cat.CategoryName, Cat.TotalCategory
ORDER BY O.OrderID DESC
DROP FUNCTION dbo.getOrderPerCategoryCost

SELECT CASE WHEN GROUPING(C.CategoryID)=1 THEN 'ALL' ELSE MIN(C.CategoryName) END CategoryName, 
    CASE WHEN GROUPING (Cus.ContactName) = 1 THEN 'ALL' ELSE MIN(Cus.ContactName) END Customer, 
    SUM(OD.UnitPrice*OD.Quantity*(1-OD.Discount)) Total
  FROM (SELECT TOP 100 * FROM Orders ORDER BY OrderDate DESC) O
	INNER JOIN Customers Cus ON Cus.CustomerID = O.CustomerID 
    INNER JOIN [Order Details] OD ON O.OrderID = OD.OrderID
    INNER JOIN Products P ON P.ProductID = OD.ProductID
    INNER JOIN Categories C ON P.CategoryID = C.CategoryID
  GROUP BY C.CategoryID, Cus.ContactName
  WITH CUBE
  ORDER BY CategoryName, Customer