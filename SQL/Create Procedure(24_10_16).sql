USE [Blog]
GO

IF OBJECT_ID('dbo.SPLIT') IS NOT NULL
  DROP FUNCTION SPLIT
GO
CREATE FUNCTION [dbo].[Split]
(
    @String NVARCHAR(4000),
    @Delimiter NCHAR(1)
)
RETURNS TABLE
AS
RETURN
(
    WITH Split(stpos,endpos)
    AS(
        SELECT 0 AS stpos, CHARINDEX(@Delimiter,@String) AS endpos
        UNION ALL
        SELECT endpos+1, CHARINDEX(@Delimiter,@String,endpos+1)
            FROM Split
            WHERE endpos > 0
    )
    SELECT 'Id' = ROW_NUMBER() OVER (ORDER BY (SELECT 1)),
        'Data' = SUBSTRING(@String,stpos,COALESCE(NULLIF(endpos,0),LEN(@String)+1)-stpos)
    FROM Split
)
GO

IF OBJECT_ID ( 'dbo.CreatePost', 'P' ) IS NOT NULL   
    DROP PROCEDURE dbo.CreatePost;  
GO
  
CREATE PROCEDURE CreatePost
	@UserId INT,
	@TextPost nvarchar(MAX),   
    @Tags nvarchar(4000)      
AS   
--DECLARE @tempId INT, @tempPost NVARCHAR(MAX), @tempTags NVARCHAR(4000)
--SET @tempId = @UserId
--SET @tempTextPost
	
	INSERT INTO Posts
	(Post, UserId)
	VALUES(@TextPost, @UserId)

INSERT INTO Tags
([Name])
SELECT Res.TagName
FROM 
(
SELECT RTRIM(LTRIM([Data])) AS TagName
FROM dbo.Split(@Tags, ',')
GROUP BY RTRIM(LTRIM([Data]))
) Res
LEFT JOIN Tags T ON T.[Name] = Res.TagName
WHERE T.Id IS NULL

INSERT INTO TagPost
(TagId, PostId)
SELECT T.Id
	, P.Id
FROM 
	(
		SELECT*
		FROM Posts Post
		WHERE Post.Post = @TextPost AND Post.UserId = @UserId
	) P
INNER JOIN Tags T ON  CHARINDEX(T.[Name], @Tags) > 0
GO

GO  
Exec CreatePost 1, 'TestPost2', 'Tags7, Tags8, work '
GO

SELECT U.[Name]
, P.Post
, T.[Name] AS Tag
FROM Posts P
INNER JOIN Users U ON U.Id = P.UserId
INNER JOIN TagPost TP ON TP.PostId = P.Id
INNER JOIN Tags T ON T.Id = TP.TagId