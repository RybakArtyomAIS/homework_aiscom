IF OBJECT_ID('GetOrderTotalView', 'V') IS NOT NULL
DROP VIEW GetOrderTotalView
GO
CREATE VIEW GetOrderTotalView
AS
SELECT dbo.getEmpoloyeeFullName(E.EmployeeID) AS [FULL NAME]
, SUM(dbo.getFullCostOrderDetail(OD.ProductID, OD.OrderID)) AS [TOTAL]
, E.HireDate
, E.Country
, E.Title
FROM Employees E
INNER JOIN Orders O ON O.EmployeeID = E.EmployeeID
INNER JOIN [Order Details] OD ON OD.OrderID = O.OrderID
INNER JOIN EmployeeTerritories ET ON ET.EmployeeID = E.EmployeeID
INNER JOIN Territories T ON T.TerritoryID = ET.TerritoryID
GROUP BY E.EmployeeID, E.Title, E.HireDate, E.Country
GO
----------------------------------------
SELECT*
FROM GetOrderTotalView Res
ORDER BY Res.[FULL NAME]


