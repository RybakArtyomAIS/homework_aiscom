USE master
GO

IF EXISTS(select * from sys.databases where name='Training_RYBAK')
DROP DATABASE Training_RYBAK
GO


CREATE DATABASE Training_RYBAK
GO
USE Training_RYBAK
GO

CREATE TABLE TypesOfMoney
(
	Id int NOT NULL PRIMARY KEY IDENTITY(1, 1),
	Name nvarchar(250) NOT NULL
)
GO

CREATE TABLE Denominations
(
	Id int NOT NULL PRIMARY KEY IDENTITY(1, 1),
	DifferenceToOne money NOT NULL,
	TypeId int NOT NULL FOREIGN KEY REFERENCES TypesOfMoney(Id)
)
GO

CREATE TABLE CrossCourses
(
	Id int NOT NULL PRIMARY KEY IDENTITY(1, 1),
	FirstType int NOT NULL FOREIGN KEY REFERENCES TypesOfMoney(Id),
	SecondTypeId int NOT NULL FOREIGN KEY REFERENCES TypesOfMoney(Id),
	[Difference] float NOT NULL 
)
GO

CREATE TABLE Accounts
(
	Id int NOT NULL PRIMARY KEY IDENTITY(1, 1),
	Total money NOT NULL default(0),
	TypeOfMoneyId int NOT NULL FOREIGN KEY REFERENCES TypesOfMoney(Id),
	CreateDate datetime NOT NULL default(GETDATE()),
	DeleteDate datetime NULL
)
GO

CREATE TABLE CategoriesOfTransaction
(
	Id int NOT NULL PRIMARY KEY IDENTITY(1, 1),
	Name varchar(250) NOT NULL
)
GO

CREATE TABLE TypesOfTransaction
(
	Id int NOT NULL PRIMARY KEY IDENTITY(1, 1),
	Name varchar(250) NOT NULL,
	CategoryId int NOT NULL FOREIGN KEY REFERENCES CategoriesOfTransaction(Id)
)
GO

CREATE TABLE Transactions
(
	Id int NOT NULL PRIMARY KEY IDENTITY(1, 1),
	Amount money NOT NULL default(0),
	TypeId int NOT NULL FOREIGN KEY REFERENCES TypesOfTransaction(Id),
	AccountId int NOT NULL FOREIGN KEY REFERENCES Accounts(Id),
	Note varchar(250) NOT NULL default(''),
	CreateDate datetime NOT NULL default(GETDATE()),
	ExecuteDate datetime NULL,
	DeleteDate datetime NULL
)
GO
