USE Blog
GO


IF OBJECT_ID('dbo.TagPost') IS NOT NULL
  DROP TABLE TagPost
GO

IF OBJECT_ID('dbo.Tags') IS NOT NULL
  DROP TABLE Tags
GO

IF OBJECT_ID('dbo.Comments') IS NOT NULL
  DROP TABLE Comments
GO

IF OBJECT_ID('dbo.Posts') IS NOT NULL
  DROP TABLE Posts
GO
IF OBJECT_ID('dbo.Users') IS NOT NULL
  DROP TABLE Users
GO

CREATE TABLE Users
(
	Id int NOT NULL PRIMARY KEY IDENTITY(1, 1),
	Name nvarchar(250) NOT NULL
	, Email NVARCHAR(250) NOT NULL
)

GO


CREATE TABLE Tags
(
	Id int NOT NULL PRIMARY KEY IDENTITY(1, 1),
	Name nvarchar(250) NOT NULL
)
GO

CREATE TABLE Posts
(
	Id int NOT NULL PRIMARY KEY IDENTITY(1, 1),
	Post nvarchar(MAX) NOT NULL
	, UserId int NOT NULL FOREIGN KEY REFERENCES Users(Id)
)
GO

CREATE TABLE TagPost
(
	TagId int NOT NULL,
	PostId int NOT NULL,
	CONSTRAINT PK_TagPost PRIMARY KEY (TagId, PostId),
	CONSTRAINT FK_Tag
    FOREIGN KEY (TagId) REFERENCES Tags (Id),
	CONSTRAINT FK_Post
    FOREIGN KEY (PostId) REFERENCES Posts (Id)
)

CREATE TABLE Comments
(
	Id int NOT NULL PRIMARY KEY IDENTITY(1, 1),
	Comment nvarchar(MAX) NOT NULL
	, UserId int NOT NULL FOREIGN KEY REFERENCES Users(Id)
	, PostId int NOT NULL FOREIGN KEY REFERENCES Posts(Id)
)

GO

INSERT INTO Users 
(Name, Email)
SELECT B.Author
, B.AuthorEmail
FROM Blog B
GROUP BY B.Author, B.AuthorEmail

GO

INSERT INTO Tags 
(Name)
SELECT B.Tag
FROM Blog B
GROUP BY B.Tag

GO

INSERT INTO Posts 
(Post, UserId)
SELECT B.Post
, U.Id
FROM Blog B
INNER JOIN Users U ON U.Name = B.Author AND U.Email = B.AuthorEmail
GROUP BY B.Post, U.Id

GO

INSERT INTO Comments 
(Comment, UserId, PostId)
SELECT B.Comment
, U.Id
, P.Id
FROM Blog B
INNER JOIN Users U ON U.Name = B.CommentAuthor
INNER JOIN Posts P ON P.Post = B.Post
GROUP BY B.Comment, U.Id, P.Id

GO

INSERT INTO TagPost
(TagId, PostId)
SELECT T.Id AS TagId, P.Id AS PostId
FROM Blog B
INNER JOIN Tags T ON T.Name = B.Tag
INNER JOIN Posts P ON P.Post = B.Post
GROUP BY T.Id, P.Id

GO
----------------------------------------------------------

-----------------------------------------------------------------

ALTER TABLE Posts
--ADD CreateDate DATETIME DEFAULT(DATEADD(DAY, -100*RAND(), GETDATE())) NOT NULL
ADD CreateDate DATETIME DEFAULT(GETDATE()) NOT NULL
GO

UPDATE Posts
SET CreateDate = Mirrow.CreateDate
FROM 
(
SELECT DATEADD(DAY, -100*P.Id, P.CreateDate) AS CreateDate
, P.Id
FROM Posts P
) Mirrow
WHERE Mirrow.Id = Posts.Id
GO

ALTER TABLE Posts
ADD EditDate DATETIME NULL
GO

--INSERT INTO Posts
--(EditDate)
--SELECT GETDATE() AS EditDate
--FROM Posts P
--GO

ALTER TABLE Posts
ADD RemoveDate DATETIME NULL
GO

ALTER TABLE Comments
ADD CreateDate DATETIME DEFAULT(GETDATE()) NOT NULL
GO

ALTER TABLE Comments
ADD EditDate DATETIME NULL
GO

ALTER TABLE Comments
ADD RemoveDate DATETIME NULL
GO

INSERT INTO Users
(Name, Email)
VALUES ('Artem', 'ARTEM@ARTEM.LOC')
GO
INSERT INTO Posts
(Post, UserId)
SELECT 'Hello World!!!!!!\N Its my first post!!!' AS POST, U.Id
FROM Users U
WHERE U.Name = 'Artem'
GO
INSERT INTO Tags
(Name)
VALUES ('ArtemTAG1')
GO
INSERT INTO TagPost
(TagId, PostId)
SELECT T.Id, P.Id
FROM Posts P
INNER JOIN Tags T ON T.Name = 'ArtemTAG'
WHERE P.Post = 'Hello World!!!!!!\N Its my first post!!!'
GO
INSERT INTO TagPost
(TagId, PostId)
SELECT T.Id, P.Id
FROM Posts P
INNER JOIN Tags T ON T.Name = 'work'
WHERE P.Post = 'Hello World!!!!!!\N Its my first post!!!'
GO
INSERT INTO Comments
(Comment, UserId, PostId)
SELECT 'COMMENT1', U.Id, P.Id
FROM Posts P, Users U
WHERE P.Post = 'Hello World!!!!!!\N Its my first post!!!' AND U.Name = 'Oleg'
GO
-----------------------------------------------------------------------------

UPDATE Posts
SET Post = 'Brave new world! CHANGE', EditDate = GETDATE()
WHERE Post = 'Brave new world!' 
GO
UPDATE Posts
SET RemoveDate = GETDATE()
WHERE Post = 'My dog' 
GO

SELECT U.Name AS USERNAME, P.Post AS POST, T.Name AS TAG, C.Comment, P.CreateDate, P.Editdate
FROM Posts P
INNER JOIN Users U ON P.UserId = U.Id
LEFT JOIN Comments C ON C.PostId = P.Id AND C.UserId = U.Id
INNER JOIN TagPost TP ON TP.PostId = P.Id
INNER JOIN Tags T ON T.Id = TP.TagId
WHERE P.RemoveDate IS NULL