CREATE DATABASE TRAINING
GO
USE TRAINING
GO

CREATE TABLE Items_N0
(
	Category nvarchar(300) NOT NULL,
	Item nvarchar(300) NOT NULL,
	Store nvarchar(300) NOT NULL
)


INSERT INTO Items_N0 (Category, Item, Store) VALUES ('Book', 'Dune, SQL for beginners', 'Head Office')
INSERT INTO Items_N0 (Category, Item, Store) VALUES ('Laptop', 'ASUS-xxx, Toshiba-xxx', 'Head Office, Some non head store')


SELECT I.Category,
	I.Item,
	I.Store	 
FROM Items_N0 I