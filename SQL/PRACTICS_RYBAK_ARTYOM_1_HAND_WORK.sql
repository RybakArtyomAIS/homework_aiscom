CREATE DATABASE PRACTICS_HAND
GO
USE PRACTICS_HAND
GO


CREATE TABLE DEPARTMENTS
(
	Id int NOT NULL PRIMARY KEY IDENTITY(1, 1),
	Name nvarchar(250) NOT NULL,
)

INSERT INTO DEPARTMENTS (Name) 
VALUES ('ASP.NET Department')

INSERT INTO DEPARTMENTS (Name) 
VALUES ('Node.js Department')

INSERT INTO DEPARTMENTS (Name) 
VALUES ('Design Department')

INSERT INTO DEPARTMENTS (Name) 
VALUES ('Sale Department')

CREATE TABLE PROJECTS
(
	Id int NOT NULL PRIMARY KEY IDENTITY(1, 1),
	Name nvarchar(250) NOT NULL,
)

INSERT INTO PROJECTS (Name) 
VALUES ('Luxedecor')

INSERT INTO PROJECTS (Name) 
VALUES ('PLA')

INSERT INTO PROJECTS (Name) 
VALUES ('Food Bank')

INSERT INTO PROJECTS (Name) 
VALUES ('QB')

INSERT INTO PROJECTS (Name) 
VALUES ('TRSRS')

INSERT INTO PROJECTS (Name) 
VALUES ('Why Go?')

CREATE TABLE COUNTRIES
(
	Id int NOT NULL PRIMARY KEY IDENTITY(1, 1),
	Name nvarchar(250) NOT NULL,
)

INSERT INTO COUNTRIES (Name) 
VALUES ('Belarus')

INSERT INTO COUNTRIES (Name) 
VALUES ('USA')

INSERT INTO COUNTRIES (Name) 
VALUES ('Ukrain')


CREATE TABLE Employees
(
	Id int NOT NULL PRIMARY KEY IDENTITY(1, 1),
	Name nvarchar(250) NOT NULL,
	Position nvarchar(250) NOT NULL,
	COUNTRYId int NOT NULL FOREIGN KEY REFERENCES COUNTRIES(Id)
)

INSERT INTO Employees (Name, Position, COUNTRYId) 
VALUES ('Alexander', 'Programmer', '1')

INSERT INTO Employees (Name, Position, COUNTRYId) 
VALUES ('Dmitry', 'Programmer', '1')

INSERT INTO Employees (Name, Position, COUNTRYId) 
VALUES ('Kevin', 'Manager', '2')

INSERT INTO Employees (Name, Position, COUNTRYId) 
VALUES ('Eugen', 'Head', '1')

INSERT INTO Employees (Name, Position, COUNTRYId) 
VALUES ('Gleb', 'Programmer', '3')

INSERT INTO Employees (Name, Position, COUNTRYId) 
VALUES ('Anastasiya', 'Designer', '1')

INSERT INTO Employees (Name, Position, COUNTRYId) 
VALUES ('Sergey', 'Head', '1')

CREATE TABLE DEPARTMENTEmployee
(
	DepartmentId int NOT NULL,
	EmployeeId int NOT NULL,
	CONSTRAINT PK_DEPARTMENTEmployee PRIMARY KEY (DepartmentId, EmployeeId),
	CONSTRAINT FK_Department
    FOREIGN KEY (DepartmentId) REFERENCES DEPARTMENTS (Id),
	CONSTRAINT FK_Employee1
    FOREIGN KEY (EmployeeId) REFERENCES Employees (Id)
)

INSERT INTO DEPARTMENTEmployee (DepartmentId, EmployeeId)
VALUES ('1', '1')

INSERT INTO DEPARTMENTEmployee (DepartmentId, EmployeeId)
VALUES ('1', '2')

INSERT INTO DEPARTMENTEmployee (DepartmentId, EmployeeId)
VALUES ('1', '4')

INSERT INTO DEPARTMENTEmployee (DepartmentId, EmployeeId)
VALUES ('1', '7')

INSERT INTO DEPARTMENTEmployee (DepartmentId, EmployeeId)
VALUES ('2', '5')

INSERT INTO DEPARTMENTEmployee (DepartmentId, EmployeeId)
VALUES ('3', '6')

INSERT INTO DEPARTMENTEmployee (DepartmentId, EmployeeId)
VALUES ('4', '3')

INSERT INTO DEPARTMENTEmployee (DepartmentId, EmployeeId)
VALUES ('1', '3')

INSERT INTO DEPARTMENTEmployee (DepartmentId, EmployeeId)
VALUES ('2', '7')

INSERT INTO DEPARTMENTEmployee (DepartmentId, EmployeeId)
VALUES ('3', '7')

INSERT INTO DEPARTMENTEmployee (DepartmentId, EmployeeId)
VALUES ('4', '7')

CREATE TABLE PROJECTSEmployee
(
	ProjectId int NOT NULL,
	EmployeeId int NOT NULL,
	CONSTRAINT PK_PROJECTSEmployee PRIMARY KEY (ProjectId, EmployeeId),
	CONSTRAINT FK_Project
    FOREIGN KEY (ProjectId) REFERENCES PROJECTS (Id),
	CONSTRAINT FK_Emplyee
    FOREIGN KEY (EmployeeId) REFERENCES EMPLOYEES (Id)
)

INSERT INTO PROJECTSEmployee (ProjectId, EmployeeId)
VALUES ('1', '1')

INSERT INTO PROJECTSEmployee (ProjectId, EmployeeId)
VALUES ('1', '2')

INSERT INTO PROJECTSEmployee (ProjectId, EmployeeId)
VALUES ('1', '3')

INSERT INTO PROJECTSEmployee (ProjectId, EmployeeId)
VALUES ('1', '6')

INSERT INTO PROJECTSEmployee (ProjectId, EmployeeId)
VALUES ('1', '7')

INSERT INTO PROJECTSEmployee (ProjectId, EmployeeId)
VALUES ('2', '5')

INSERT INTO PROJECTSEmployee (ProjectId, EmployeeId)
VALUES ('2', '6')

INSERT INTO PROJECTSEmployee (ProjectId, EmployeeId)
VALUES ('2', '7')

INSERT INTO PROJECTSEmployee (ProjectId, EmployeeId)
VALUES ('3', '1')

INSERT INTO PROJECTSEmployee (ProjectId, EmployeeId)
VALUES ('3', '6')

INSERT INTO PROJECTSEmployee (ProjectId, EmployeeId)
VALUES ('3', '7')

INSERT INTO PROJECTSEmployee (ProjectId, EmployeeId)
VALUES ('4', '4')

INSERT INTO PROJECTSEmployee (ProjectId, EmployeeId)
VALUES ('4', '6')

INSERT INTO PROJECTSEmployee (ProjectId, EmployeeId)
VALUES ('4', '7')

INSERT INTO PROJECTSEmployee (ProjectId, EmployeeId)
VALUES ('5', '1')

INSERT INTO PROJECTSEmployee (ProjectId, EmployeeId)
VALUES ('5', '7')

INSERT INTO PROJECTSEmployee (ProjectId, EmployeeId)
VALUES ('6', '4')

INSERT INTO PROJECTSEmployee (ProjectId, EmployeeId)
VALUES ('6', '6')

INSERT INTO PROJECTSEmployee (ProjectId, EmployeeId)
VALUES ('6', '7')
--DROP TABLE EMPLOYEES