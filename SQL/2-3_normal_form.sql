USE TRAINING;
GO

CREATE TABLE Categories
(
	Id int NOT NULL PRIMARY KEY IDENTITY(1, 1),
	Name nvarchar(250) NOT NULL
)

GO

CREATE TABLE Items
(
	Id int NOT NULL PRIMARY KEY IDENTITY(1, 1),
	Name nvarchar(250) NOT NULL,
	CategoryId int NOT NULL FOREIGN KEY REFERENCES Categories(Id)
)

GO

CREATE TABLE Storage
(
	Id int NOT NULL PRIMARY KEY IDENTITY(1, 1),
	Name nvarchar(250) NOT NULL
)

GO

CREATE TABLE ItemStore
(
	ItemId int NOT NULL,
	StoreId int NOT NULL,
	CONSTRAINT PK_ItemStore PRIMARY KEY (ItemId, StoreId),
	CONSTRAINT FK_Item
    FOREIGN KEY (ItemId) REFERENCES Items (Id),
	CONSTRAINT FK_Store
    FOREIGN KEY (StoreId) REFERENCES Storage (Id)
)

GO

INSERT INTO Categories 
(Name)
SELECT I_N1.Category 
FROM Items_N1 I_N1
GROUP BY I_N1.Category 

GO

INSERT INTO Storage
(Name)
SELECT I_N1.Store 
FROM Items_N1 I_N1
GROUP BY I_N1.Store

GO

INSERT INTO Items
(Name, CategoryId)
SELECT I_N1.Item, C.Id
FROM Items_N1 I_N1
INNER JOIN Categories C ON C.Name = I_N1.Category
GROUP BY I_N1.Item, I_N1.Category, C.Id  

GO

INSERT INTO ItemStore
(ItemId, StoreId)
SELECT I.Id AS ItemId, S.Id AS StoreId
FROM Items_N1 I_N1
INNER JOIN Items I ON I_N1.Item = I.Name
INNER JOIN Storage S ON I_N1.Store = S.Name

GO

SELECT C.Name AS CategoryName,
	I.Name AS ItemName,	
	S.Name AS StoreName
FROM Items I
INNER JOIN Categories C ON I.CategoryId = C.Id
INNER JOIN ItemStore [IS] ON I.Id = [IS].ItemId
INNER JOIN Storage S ON [IS].StoreId = S.Id


--DROP TABLE ItemStore, Items, Categories, Storage