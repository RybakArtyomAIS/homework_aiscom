USE TRAINING;
GO

CREATE TABLE Items_N1
(
	Category nvarchar(300) NOT NULL,
	Item nvarchar(300) NOT NULL,
	Store nvarchar(300) NOT NULL
)

INSERT INTO Items_N1 (Category, Item, Store) VALUES ('Book', 'Dune', 'Head Office')
INSERT INTO Items_N1 (Category, Item, Store) VALUES ('Book', 'SQL for beginners', 'Head Office')
INSERT INTO Items_N1 (Category, Item, Store) VALUES ('Laptop', 'ASUS-xxx', 'Some non head store')
INSERT INTO Items_N1 (Category, Item, Store) VALUES ('Laptop', 'ASUS-xxx', 'Head Office')
INSERT INTO Items_N1 (Category, Item, Store) VALUES ('Laptop', 'Toshiba-xxx', 'Some non head store')
INSERT INTO Items_N1 (Category, Item, Store) VALUES ('Laptop', 'Toshiba-xxx', 'Head Office')


SELECT I.Category,
	I.Item,
	I.Store	 
FROM Items_N1 I