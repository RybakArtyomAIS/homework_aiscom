SELECT C.ContactName
	, O.OrderDate,
	O.OrderID
FROM Customers C
LEFT OUTER JOIN Orders O ON C.CustomerID = O.CustomerID
--WHERE O.OrderDate IS NOT NULL
--WHERE O.OrderDate IS NULL
----------------------------------------------------------------
SELECT CompanyName, Phone
FROM Shippers
	UNION 
SELECT CompanyName, Phone
FROM Customers
	UNION 
SELECT CompanyName, Phone
FROM Suppliers
ORDER BY CompanyName
-----------------------------------------------------------------

SELECT C.CustomerID
	, C.ContactName 
	, COUNT(O.CustomerID) AS OrderCount
FROM Orders O
INNER JOIN Customers C ON O.CustomerID = C.CustomerID
GROUP BY C.CustomerID, C.ContactName 
-------------------------------------------------------------------

SELECT OrderId
	, SUM(UnitPrice*Quantity) AS Total
FROM [Order Details] OD
GROUP BY OrderID
ORDER BY Total DESC
-----------------------------------------------------------------------
SELECT C.ContactName
		, CO.OrderID
		, CO.OrderDate
FROM Customers C
CROSS APPLY(SELECT TOP 2 O.OrderID
					, O.OrderDate
			FROM Orders O 
			WHERE O.CustomerID = C.CustomerID
			ORDER BY O.OrderDate DESC) AS CO
ORDER BY ContactName ASC, OrderDate DESC
---------------------------------------------------------
SELECT OrderId
	--, SUM(UnitPrice*Quantity) AS Total
FROM [Order Details] OD
GROUP BY OrderID
HAVING SUM(UnitPrice*Quantity) > 7000

------------------------------------------------------------

SELECT Cust.ContactName
	, Cust.CustomerID
	, Cust.Num
FROM (SELECT ROW_NUMBER() OVER(ORDER BY C.CustomerId) AS Num
		, C.CustomerID
		, c.ContactName
	FROM Customers C) AS Cust
WHERE Cust.Num BETWEEN 5 AND 45
ORDER BY NUM, ContactName ASC
--------------------------------------------------------------------

SELECT Cust.CompanyName
	, Cust.ContactName
	, Cust.Number
	, Cust.OrderID
	, Cust.Total
FROM (SELECT C.ContactName
	, C.CompanyName
	, O.OrderID 
	, (OD.Quantity * OD.UnitPrice) AS Total
	, ROW_NUMBER() OVER(PARTITION BY C.CompanyName, C.ContactName ORDER BY C.CustomerId) AS Number
FROM Customers C
INNER JOIN Orders O ON O.CustomerID = C.CustomerID
INNER JOIN [Order Details] OD ON OD.OrderID = O.OrderID) AS Cust
WHERE Cust.Number <= 3