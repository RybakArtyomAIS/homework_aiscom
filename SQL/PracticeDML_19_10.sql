----------------------------------------------------------------------- Part A

-- For each customer select all his orders, including product info and full employee info
-- For each customer select Total of all his orders (do not forget about discount)
-- Create a report showing the contact name and phone numbers for all employees, customers, and suppliers.
-- Select average total (do not forget about discount) for each customer if average total less then 10 or more then 25.
-- Find the Companies that placed orders in 1997
-- Find the name of the company that placed order 10290 WITHOUT JOIN statement. Try to use sub select.
-- Select all customer with orders. If customer hasn't order - set 'No Order' instead of NULL.
-- Select 3 customers with orders total greater then others
-- Select Customers with total for each order beetween 5-10 positions in the result
-- Create View shows top 5 (or less) best selling products for each year.


---------------------------------------------------------------- Part B
--Show 10 latest orders for each customer which CustomerID starts with 'B'

--Show minimum, maximum and average order cost for 10 customers with at least one order, but with minimum number of orders.

--Find total amount of all orders for each customer, sort customers by the value from maximum amount to minimum and show customers from 40 to 50 position in the result. 

--Show best sellers: top 10 catories and top 3 products in each category with the highest volume of sales per product and per category.

--Show sales volume for each employee for the 1997 year month by month. Result should contain 108 records (9*12)

--For 100 latest orders show sales volume per customer and category. Besides that the result should contain sales volume per category for all customers and per customer for all categories as well as sales volume for all customers and categories. see example
-------------------------------------------------------------------------------------