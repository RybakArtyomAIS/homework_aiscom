﻿using ORM.BL;
using ORM.BL.TransferInfrastructure;
using ORM.DAL.ADONET;
using ORM.DAL.MongoDB;
using ORM.DAL.EF;
using ORM.DAL.TransferInfrastructure;
using SimpleInjector;

namespace ORM.IoC
{
    public static class IoContainer
    {
        private static Container _currentContainer;

        public static void SetMongoConfig()
        {
            _currentContainer = new Container();

            _currentContainer.Register<IAccountService, AccountService>();
            _currentContainer.Register<IRepository, MongoRepository>();

            _currentContainer.Verify();
        }

        public static void SetAdoConfig()
        {
            _currentContainer = new Container();

            _currentContainer.Register<IAccountService, AccountService>();
            _currentContainer.Register<IRepository, AdoRepository>();

            _currentContainer.Verify();
        }

        public static void SetEFConfig()
        {
            _currentContainer = new Container();

            _currentContainer.Register<IAccountService, AccountService>();
            _currentContainer.Register<IRepository, EFRepository>();

            _currentContainer.Verify();
        }

        public static IAccountService GetIAccountServiceInstance
        {
            get { return _currentContainer.GetInstance<IAccountService>(); }
        }
    }
}
