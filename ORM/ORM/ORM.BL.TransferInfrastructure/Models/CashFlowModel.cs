﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ORM.BL.TransferInfrastructure.Models
{
    public class CashFlowModel
    {
        public decimal UsdBalance { get; set; }
        public decimal BlrBalance { get; set; }
        public decimal OldBlrBalance { get; set; }
        public DateTime Date { get; set; }
    }
}
