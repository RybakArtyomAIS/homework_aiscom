﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ORM.DAL.TransferInfrastructure.Models;

namespace ORM.BL.TransferInfrastructure.Models
{
    public static class ExtensionsMethods
    {
        public static AccountDAL ToDAL(this AccountModel model)
        {
            var result = new AccountDAL
            {
                Id = model.Id,
                Amount = model.Amount,
                TypeOfMoney = model.TypeOfMoney.ToDAL()
            };
            result.TransactionsList = model.TransactionList.Select(item => new TransactionDAL
            {
                Id = item.Id,
                Date = item.Date,
                Total = item.Total,
                Account = result,
                TypeOfTransaction = item.TypeOfTransactionModel.ToDAL()
            }).ToList();
            return result;
        }

        public static AccountModel ToModel(this AccountDAL dal)
        {
            var result = new AccountModel
            {
                Id = dal.Id,
                Amount = dal.Amount,
                TypeOfMoney = dal.TypeOfMoney.ToModel()
            };
            result.TransactionList = dal.TransactionsList.Select(item => new TransactionModel
            {
                Id = item.Id,
                Date = item.Date,
                Total = item.Total,
                AccountModel = result,
                TypeOfTransactionModel = item.TypeOfTransaction.ToModel()
            }).ToList();
            return result;
        }

        public static TransactionDAL ToDAL(this TransactionModel model)
        {
            return new TransactionDAL
            {
                Id = model.Id,
                Date = model.Date,
                Total = model.Total,
                Account = model.AccountModel.ToDAL(),
                TypeOfTransaction = model.TypeOfTransactionModel.ToDAL()
            };
        }

        public static TransactionModel ToModel(this TransactionDAL dal)
        {
            return new TransactionModel
            {
                Id = dal.Id,
                Date = dal.Date,
                Total = dal.Total, 
                AccountModel = dal.Account.ToModel(),
                TypeOfTransactionModel = dal.TypeOfTransaction.ToModel()
            };
        }


        public static TypeOfTransactionDAL ToDAL(this TypeOfTransactionModel model)
        {
            return new TypeOfTransactionDAL
            {
                Id = model.Id,
                Name = model.Name,
                CategoryOfTransaction = model.CategoryOfTransactionModel.ToDAL()
            };
        }

        public static TypeOfTransactionModel ToModel(this TypeOfTransactionDAL dal)
        {
            return new TypeOfTransactionModel
            {
                Id = dal.Id,
                Name = dal.Name,
                CategoryOfTransactionModel = dal.CategoryOfTransaction.ToModel()
            };
        }


        public static CategoryOfTransactionDAL ToDAL(this CategoryOfTransactionModel model)
        {
            return new CategoryOfTransactionDAL
            {
                Id = model.Id,
                Name = model.Name
            };
        }

        public static CategoryOfTransactionModel ToModel(this CategoryOfTransactionDAL dal)
        {
            return new CategoryOfTransactionModel
            {
                Id = dal.Id,
                Name = dal.Name
            };
        }


        public static TypeOfMoneyDAL ToDAL(this TypeOfMoneyModel model)
        {
            var result = new TypeOfMoneyDAL
            {
                Id = model.Id,
                Name = model.Name
            };
            result.CrossCourseList = model.CrossCourseList.Select(item => new CrossCourseDAL
            {
                Id = item.Id,
                Difference = item.Difference,
                Date = item.Date,
                FirstTypeOfMoney = result, 
                SecondTypeOfMoneyId = item.SecondTypeOfMoneyId, 
                SecondTypeOfMoneyName = item.SecondTypeOfMoneyName
            }).ToList();
            result.DenominationList = model.DenominationList.Select(item => new DenominationDAL
            {
                Id = item.Id,
                DifferenceToOne = item.DifferenceToOne,
                TypeOfMoney = result
            }).ToList();
            return result;
        }

        public static TypeOfMoneyModel ToModel(this TypeOfMoneyDAL dal)
        {
            var result = new TypeOfMoneyModel
            {
                Id = dal.Id,
                Name = dal.Name
            };
            result.CrossCourseList = dal.CrossCourseList.Select(item => new CrossCourseModel
            {
                Id = item.Id,
                Difference = item.Difference,
                Date = item.Date,
                FirstTypeOfMoneyModel = result, 
                SecondTypeOfMoneyId = item.SecondTypeOfMoneyId, 
                SecondTypeOfMoneyName = item.SecondTypeOfMoneyName
            }).ToList();
            result.DenominationList = dal.DenominationList.Select(item => new DenominationModel
            {
                Id = item.Id,
                DifferenceToOne = item.DifferenceToOne,
                TypeOfMoneyModel = result
            }).ToList();
            return result;
        }


        public static CrossCourseDAL ToDAL(this CrossCourseModel model)
        {
            return new CrossCourseDAL
            {
                Id = model.Id,
                Difference = model.Difference,
                Date = model.Date,
                FirstTypeOfMoney = model.FirstTypeOfMoneyModel.ToDAL(), 
                SecondTypeOfMoneyId = model.SecondTypeOfMoneyId, 
                SecondTypeOfMoneyName = model.SecondTypeOfMoneyName
            };
        }

        public static CrossCourseModel ToModel(this CrossCourseDAL dal)
        {
            return new CrossCourseModel
            {
                Id = dal.Id,
                Difference = dal.Difference,
                Date = dal.Date,
                FirstTypeOfMoneyModel = dal.FirstTypeOfMoney.ToModel(), 
                SecondTypeOfMoneyId = dal.SecondTypeOfMoneyId, 
                SecondTypeOfMoneyName = dal.SecondTypeOfMoneyName
            };
        }


        public static DenominationDAL ToDAL(this DenominationModel model)
        {
            return new DenominationDAL
            {
                Id = model.Id,
                DifferenceToOne = model.DifferenceToOne,
                TypeOfMoney = model.TypeOfMoneyModel.ToDAL()
            };
        }

        public static DenominationModel ToModel(this DenominationDAL dal)
        {
            return new DenominationModel
            {
                Id = dal.Id,
                DifferenceToOne = dal.DifferenceToOne,
                TypeOfMoneyModel = dal.TypeOfMoney.ToModel()
            };
        }

        public static CashFlowItemDAL ToDAL(this CashFlowModel model)
        {
            return new CashFlowItemDAL
            {
                Date = model.Date,
                UsdBalance = model.UsdBalance,
                BlrBalance = model.BlrBalance,
                OldBlrBalance = model.OldBlrBalance
            };
        }

        public static CashFlowModel ToModel(this CashFlowItemDAL dal)
        {
            return new CashFlowModel
            {
                Date = dal.Date,
                UsdBalance = dal.UsdBalance,
                BlrBalance = dal.BlrBalance,
                OldBlrBalance = dal.OldBlrBalance
            };
        }

    }
}
