﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORM.BL.TransferInfrastructure.Models
{
    public class DenominationModel
    {
        public int Id { get; set; }

        public decimal DifferenceToOne { get; set; }

        public TypeOfMoneyModel TypeOfMoneyModel { get; set; }
    }
}
