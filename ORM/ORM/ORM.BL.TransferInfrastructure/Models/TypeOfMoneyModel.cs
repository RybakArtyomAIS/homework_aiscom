﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORM.BL.TransferInfrastructure.Models
{
    public class TypeOfMoneyModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public List<DenominationModel> DenominationList { get; set; }


        public List<CrossCourseModel> CrossCourseList { get; set; }
    }
}
