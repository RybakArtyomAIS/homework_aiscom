﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORM.BL.TransferInfrastructure.Models
{
    public interface ITypeOfMoneyBL
    {
        int Id { get; set; }
        string Name { get; set; }
        List<IDenominationBL> DenominationList { get; set; }
        List<ICrossCourseBL> CrossCourseList { get; set; }
    }
}
