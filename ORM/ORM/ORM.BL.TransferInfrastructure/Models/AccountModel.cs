﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ORM.BL.TransferInfrastructure.Models
{
    public class AccountModel
    {
        public int Id { get; set; }
        public decimal Amount { get; set; }
        public TypeOfMoneyModel TypeOfMoney { get; set; }
        public List<TransactionModel> TransactionList { get; set; }
    }
}
