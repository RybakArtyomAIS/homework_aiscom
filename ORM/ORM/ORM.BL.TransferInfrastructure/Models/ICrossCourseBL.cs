﻿namespace ORM.BL.TransferInfrastructure.Models
{
    public interface ICrossCourseBL
    {
        int Id { get; set; }
        double Difference { get; set; }
        ITypeOfMoneyBL FirstTypeOfMoney { get; set; }
        ITypeOfMoneyBL SecondTypeOfMoney { get; set; }
    }
}