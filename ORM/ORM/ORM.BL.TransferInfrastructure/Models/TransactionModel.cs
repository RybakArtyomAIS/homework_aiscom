﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ORM.BL.TransferInfrastructure.Models;

namespace ORM.BL.TransferInfrastructure.Models
{
    public class TransactionModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Total { get; set; }
        public DateTime Date { get; set; }

        public TypeOfTransactionModel TypeOfTransactionModel { get; set; }

        public AccountModel AccountModel { get; set; }
    }
}
