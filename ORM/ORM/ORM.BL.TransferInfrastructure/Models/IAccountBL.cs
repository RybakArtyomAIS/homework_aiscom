﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORM.BL.TransferInfrastructure.Models
{
    public interface IAccountBL
    {
        int Id { get; set; }
        decimal Amount { get; set; }
        ITypeOfMoneyBL TypeOfMoney { get; set; }
        List<ITransactionBL> TransactionList { get; set; }
    }
}
