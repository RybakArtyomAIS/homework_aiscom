﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORM.BL.TransferInfrastructure.Models
{
    public class TypeOfTransactionModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public CategoryOfTransactionModel CategoryOfTransactionModel { get; set; }
    }
}
