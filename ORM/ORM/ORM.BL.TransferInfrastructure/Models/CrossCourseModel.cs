﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ORM.BL.TransferInfrastructure.Models
{
    public class CrossCourseModel
    {
        public int Id { get; set; }
        public double Difference { get; set; }
        public DateTime Date { get; set; }

        public TypeOfMoneyModel FirstTypeOfMoneyModel { get; set; }

        public int SecondTypeOfMoneyId { get; set; }

        public string SecondTypeOfMoneyName { get; set; }

    }
}
