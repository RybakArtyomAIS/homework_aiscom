﻿using ORM.BL.TransferInfrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ORM.DAL.TransferInfrastructure;

namespace ORM.BL.TransferInfrastructure
{
    public interface IAccountService
    {
        List<CrossCourseModel> GetCrossCourses();
        void SetCrossCourse(CrossCourseModel course);

        List<DenominationModel> GetDenomination();
        void SetDenomination(DenominationModel denomination);

        List<TypeOfMoneyModel> GetTypesOfMoney();
        void SetTypeOfMoney(TypeOfMoneyModel typeOfMoney);

        List<TransactionModel> GetTransactions(int? accountId);
        void SetTransaction(TransactionModel transaction);

        List<TypeOfTransactionModel> GetTypesTransaction();
        void SetTypeOfTransaction(TypeOfTransactionModel transaction);

        List<CategoryOfTransactionModel> GetCategoriesOfTransaction();
        void SetCategoryOfTransaction(CategoryOfTransactionModel categoryOfTransaction);

        List<AccountModel> GetAccounts();
        void SetAccount(AccountModel account);

        List<CashFlowModel> GetCashFlowView();
    }
}
