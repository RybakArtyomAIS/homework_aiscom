﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace ORM.DAL
{
    public class Class1
    {
        public List<CashFlowAdoModel> ExecuteCommand()
        {
            var result = new List<CashFlowAdoModel>();
            var dp = ConfigurationManager.AppSettings["provider"];
            var cnStr = ConfigurationManager.AppSettings["cnStr"];

            var df = DbProviderFactories.GetFactory(dp);

            using (var cn = df.CreateConnection())
            {
                cn.ConnectionString = cnStr;
                cn.Open();

                var cmd = df.CreateCommand();
                cmd.Connection = cn;
                cmd.CommandText = "SELECT* FROM cashFlowView ORDER BY [Date]";
                using (var dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        result.Add(new CashFlowAdoModel
                        {
                            BlrBalance = dr["BlrBalance"].ToString(),
                            Date = dr["Date"].ToString(),
                            OldBlrBalance = dr["OldBlrBalance"].ToString(),
                            UsdBalance = dr["UsdBalance"].ToString()
                        });
                    }
                }
                return result;
            }
        }
    }
}
