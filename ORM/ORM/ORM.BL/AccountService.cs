﻿using System.Collections.Generic;
using System.Linq;
using ORM.BL.TransferInfrastructure;
using ORM.BL.TransferInfrastructure.Models;
using ORM.DAL.TransferInfrastructure;

namespace ORM.BL
{
    public class AccountService : IAccountService
    {
        private readonly IRepository _repository;
        
        public AccountService(IRepository repository)
        {
            _repository = repository;
        }

        public List<AccountModel> GetAccounts()
        {
            var result = new List<AccountModel>();
            var accounts = _repository.GetAccounts();
            result.AddRange(accounts.Select(item => item.ToModel()));
            return result;
        }

        public List<TypeOfMoneyModel> GetTypesOfMoney()
        {
            var result = new List<TypeOfMoneyModel>();
            var types = _repository.GetTypesOfMoney();
            result.AddRange(types.Select(item => item.ToModel()));
            return result;
        }

        public List<CashFlowModel> GetCashFlowView()
        {
            var result = new List<CashFlowModel>();
            var view = _repository.GetCashFlowView();
            result.AddRange(view.Select(item => item.ToModel()));
            return result;
        }

        public void SetAccount(AccountModel account)
        {
            _repository.SetAccount(account.ToDAL());
        }

        public void SetTypeOfMoney(TypeOfMoneyModel typeOfMoney)
        {
            _repository.SetTypeOfMoney(typeOfMoney.ToDAL());
        }

        #region Contains of throw new System.NotImplementedException();

        public List<CrossCourseModel> GetCrossCourses()
        {
            throw new System.NotImplementedException();
        }

        public void SetCrossCourse(CrossCourseModel course)
        {
            throw new System.NotImplementedException();
        }

        public List<DenominationModel> GetDenomination()
        {
            throw new System.NotImplementedException();
        }

        public void SetDenomination(DenominationModel denomination)
        {
            throw new System.NotImplementedException();
        }

        public List<TransactionModel> GetTransactions(int? accountId)
        {
            throw new System.NotImplementedException();
        }

        public void SetTransaction(TransactionModel transaction)
        {
            throw new System.NotImplementedException();
        }

        public List<TypeOfTransactionModel> GetTypesTransaction()
        {
            throw new System.NotImplementedException();
        }

        public void SetTypeOfTransaction(TypeOfTransactionModel transaction)
        {
            throw new System.NotImplementedException();
        }

        public List<CategoryOfTransactionModel> GetCategoriesOfTransaction()
        {
            throw new System.NotImplementedException();
        }

        public void SetCategoryOfTransaction(CategoryOfTransactionModel categoryOfTransaction)
        {
            throw new System.NotImplementedException();
        }

        #endregion
    }
}
