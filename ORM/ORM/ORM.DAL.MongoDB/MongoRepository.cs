﻿using MongoDB.Driver;
using System.Linq;
using System.Configuration;
using ORM.DAL.MongoDB.Models;
using System.Threading.Tasks;
using ORM.DAL.TransferInfrastructure;
using ORM.DAL.TransferInfrastructure.Models;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using System.Collections.Generic;

namespace ORM.DAL.MongoDB
{
    public class MongoRepository : IRepository
    {
        private readonly IMongoDatabase _database;
        public MongoRepository()
        {
            var client = new MongoClient(ConfigurationManager.ConnectionStrings["MongoDb"].ConnectionString);
            _database = client.GetDatabase("Training_Rybak");
            DropCollection("Accounts");
            DropCollection("TypesOfMoney");
        }

        public void SetAccount(AccountDAL account)
        {
            AddAccount(account.ToMongo());
        }

        private void AddAccount(AccountMongo account)
        {
            var accountList = _database.GetCollection<AccountMongo>("Accounts");
            accountList.InsertOne(account);
        }

        public void SetTypeOfMoney(TypeOfMoneyDAL typeOfMoney)
        {
            AddTypeOfMoney(typeOfMoney.ToMongo());
        }

        public List<CashFlowItemDAL> GetCashFlowView()
        {
            var result = new List<CashFlowItemDAL>();
            var typeOfMoneyCollection = _database.GetCollection<TypeOfMoneyMongo>("TypesOfMoney");
            var accountCollection = _database.GetCollection<AccountMongo>("Accounts");
            //var filter = Builders<BsonDocument>.Filter.Eq("Name", "Usd");
            //var test = typeOfMoneyCollection.Find(filter).ToList();
            var crossCourses = typeOfMoneyCollection.Find(new BsonDocument()).First().CrossCourses.OrderBy(c => c.Date);
            var brTransactions = accountCollection.Find(a => a.TypeOfMoney == "Br").First().Transactions.ToList();
            var usdTransactions = accountCollection.Find(a => a.TypeOfMoney == "Usd").First().Transactions.ToList();
                
            var currentTime = crossCourses.First().Date;
            var endTime = crossCourses.Last().Date;
            decimal usdBalance = 0;
            decimal blrBalance = 0;
            var denomination = typeOfMoneyCollection.Find(t => t.Name == "Br").First().Denominations.First().DifferenceToOne;
            while (currentTime <= endTime)
            {
                usdBalance += usdTransactions.Where(item => item.Type == "Inc" && item.Date == currentTime).Sum(item => item.Amount);
                usdBalance -= usdTransactions.Where(item => item.Type == "Exp" && item.Date == currentTime).Sum(item => item.Amount);
                blrBalance += brTransactions.Where(item => item.Type == "Inc" && item.Date == currentTime).Sum(item => item.Amount);
                blrBalance -= brTransactions.Where(item => item.Type == "Exp" && item.Date == currentTime).Sum(item => item.Amount);
                result.Add(new CashFlowItemDAL
                {
                    Date = currentTime,
                    BlrBalance = blrBalance,
                    UsdBalance = usdBalance,
                    OldBlrBalance = blrBalance * denomination
                });
                currentTime = currentTime.AddDays(1);
            }
            return result;
        }

        private void AddTypeOfMoney(TypeOfMoneyMongo typeOfMoney)
        {
            var accountList = _database.GetCollection<TypeOfMoneyMongo>("TypesOfMoney");
            accountList.InsertOne(typeOfMoney);
        }

        private void CreateIfNotExists(string collectionName)
        {
            var collection = _database.GetCollection<BsonDocument>(collectionName);
            var filter = new BsonDocument();
            if (collection.Count(filter) == 0)
            {
                _database.CreateCollection(collectionName);
            }
        }

        private void DropCollection(string name)
        {
            var collection = _database.GetCollection<BsonDocument>(name);
            var filter = new BsonDocument();
            collection.DeleteMany(filter);
            //CreateIfNotExists(name);
        }

        #region Contains of throw new System.NotImplementedException();

        public System.Collections.Generic.List<CrossCourseDAL> GetCrossCourses()
        {
            throw new System.NotImplementedException();
        }

        public void SetCrossCourse(CrossCourseDAL course)
        {
            throw new System.NotImplementedException();
        }

        public System.Collections.Generic.List<DenominationDAL> GetDenomination()
        {
            throw new System.NotImplementedException();
        }

        public void SetDenomination(DenominationDAL denomination)
        {
            throw new System.NotImplementedException();
        }

        public System.Collections.Generic.List<TypeOfMoneyDAL> GetTypesOfMoney()
        {
            throw new System.NotImplementedException();
        }

        public System.Collections.Generic.List<TransactionDAL> GetTransactions(int? accountId)
        {
            throw new System.NotImplementedException();
        }

        public void SetTransaction(TransactionDAL transaction)
        {
            throw new System.NotImplementedException();
        }

        public System.Collections.Generic.List<TypeOfTransactionDAL> GetTypesTransaction()
        {
            throw new System.NotImplementedException();
        }

        public void SetTypeOfTransaction(TypeOfTransactionDAL transaction)
        {
            throw new System.NotImplementedException();
        }

        public System.Collections.Generic.List<CategoryOfTransactionDAL> GetCategoriesOfTransaction()
        {
            throw new System.NotImplementedException();
        }

        public void SetCategoryOfTransaction(CategoryOfTransactionDAL categoryOfTransaction)
        {
            throw new System.NotImplementedException();
        }

        public System.Collections.Generic.List<AccountDAL> GetAccounts()
        {
            throw new System.NotImplementedException();
        }
        #endregion
    }
}
