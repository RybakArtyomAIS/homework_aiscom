﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ORM.DAL.TransferInfrastructure.Models;

namespace ORM.DAL.MongoDB.Models
{
    public static class ExtensionMethods
    {
        public static AccountMongo ToMongo(this AccountDAL dal)
        {
            return new AccountMongo 
            {
                Id = dal.Id, 
                Total = dal.Amount, 
                TypeOfMoney = dal.TypeOfMoney.Name, 
                TypeOfMoneyId = dal.TypeOfMoney.Id, 
                Transactions = dal.TransactionsList.Select(ToMongo).ToList()
            };
        }

        public static AccountDAL ToDAL(this AccountMongo mongo)
        {
            throw new Exception("Mongo to DAL");
            //return new AccountDAL
            //{

            //};
        }

        public static CrossCourseMongo ToMongo(this CrossCourseDAL dal)
        {
            return new CrossCourseMongo
            {
                Id = dal.Id, 
                Date = dal.Date.ToLocalTime(), 
                Difference = dal.Difference, 
                SecondTypeOfMoneyId = dal.SecondTypeOfMoneyId,
                SecondTypeOfMoneyName = dal.SecondTypeOfMoneyName
            };
        }

        public static CrossCourseDAL ToDAL(this CrossCourseMongo mongo)
        {
            throw new Exception("Mongo to DAL");
            //return new CrossCourseDAL
            //{

            //};
        }

        public static DenominationMongo ToMongo(this DenominationDAL dal)
        {
            return new DenominationMongo
            {
                Id = dal.Id,
                DifferenceToOne = dal.DifferenceToOne
            };
        }

        public static DenominationDAL ToDAL(this DenominationMongo mongo)
        {
            throw new Exception("Mongo to DAL");
            //return new DenominationDAL
            //{

            //};
        }

        public static TransactionMongo ToMongo(this TransactionDAL dal)
        {
            return new TransactionMongo
            {
                Id = dal.Id, 
                Amount = dal.Total,
                Date = dal.Date.ToLocalTime(), 
                Name = dal.TypeOfTransaction.Name, 
                Type = dal.TypeOfTransaction.CategoryOfTransaction.Name
            };
        }

        public static TransactionDAL ToDAL(this TransactionMongo mongo)
        {
            throw new Exception("Mongo to DAL");
            //return new TransactionDAL
            //{

            //};
        }

        public static TypeOfMoneyMongo ToMongo(this TypeOfMoneyDAL dal)
        {
            return new TypeOfMoneyMongo
            {
                Id = dal.Id, 
                Name = dal.Name, 
                CrossCourses = dal.CrossCourseList.Select(item => item.ToMongo()).ToList(),
                Denominations = dal.DenominationList.Select(item => item.ToMongo()).ToList()
            };
        }

        public static TypeOfMoneyDAL ToDAL(this TypeOfMoneyMongo mongo)
        {
            throw new Exception("Mongo to DAL");
            //return new TypeOfMoneyDAL
            //{

            //};
        }


    }
}
