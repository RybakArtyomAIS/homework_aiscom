﻿using System;

namespace ORM.DAL.MongoDB.Models
{
    public class TransactionMongo
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public decimal Amount { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
    }
}
