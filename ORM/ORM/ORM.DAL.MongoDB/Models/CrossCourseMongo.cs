﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ORM.DAL.MongoDB.Models
{
    public class CrossCourseMongo
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public double Difference { get; set; }
        public int SecondTypeOfMoneyId { get; set; }
        public string SecondTypeOfMoneyName { get; set; }
    }
}
