﻿using System;
using System.Linq;
using ORM.IoC;

namespace ORM.ConsoleApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            //IoContainer.SetAdoConfig();
            //var accountServiceAdo = IoContainer.GetIAccountServiceInstance;
            IoContainer.SetEFConfig();
            var accountServiceEF = IoContainer.GetIAccountServiceInstance;
            IoContainer.SetMongoConfig();
            var accountServiceMongo = IoContainer.GetIAccountServiceInstance;
            var accounts = accountServiceEF.GetAccounts();
            foreach (var account in accounts)
            {
                accountServiceMongo.SetAccount(account);
                Console.WriteLine("Set account to mongo ID {0}, Amount {1} {2}, Count of transactions {3}", account.Id, account.Amount, account.TypeOfMoney.Name, account.TransactionList.Count());
            }

            var typeOfMoneyList = accountServiceEF.GetTypesOfMoney();
            foreach (var type in typeOfMoneyList)
            {
                accountServiceMongo.SetTypeOfMoney(type);
                Console.WriteLine("Set type of money to mongo ID {0}, Name {1}, Denomination {2}", type.Id, type.Name, type.DenominationList.Any() ? "Yes" : "No");
            }
            var cashFlowView = accountServiceEF.GetCashFlowView();
            //var cashFlowView = accountServiceMongo.GetCashFlowView();

            Console.SetBufferSize(Console.BufferWidth, cashFlowView.Count() + accounts.Count() + typeOfMoneyList.Count());
            foreach(var cashFlowItem in cashFlowView)
            {
                Console.WriteLine("{0}, USD Balance {1}, Blr Balance {2}, Old Blr Balance {3}", cashFlowItem.Date, cashFlowItem.UsdBalance, cashFlowItem.BlrBalance, cashFlowItem.OldBlrBalance);
            }
            Console.ReadKey();
        }
    }
}
