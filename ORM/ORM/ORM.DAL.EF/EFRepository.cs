﻿using ORM.DAL.TransferInfrastructure;
using ORM.DAL.TransferInfrastructure.Models;
using ORM.DAL.EF.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace ORM.DAL.EF
{
    public class EFRepository : IRepository
    {
        private readonly AccountContext _accountContext;
        public EFRepository()
        {
            _accountContext = new AccountContext();
        }

        public List<TypeOfMoneyDAL> GetTypesOfMoney()
        {
            //var test = _accountContext.TypesOfMoney.Include(t => t.Denominations).Include(t => t.CrossCourses).Include(t => t.Accounts).ToList();
            //return test.Select(t => t.ToDAL()).ToList();
            var typesOfMoney = _accountContext.TypesOfMoney.ToList();
            return typesOfMoney.Select(t => t.ToDAL()).ToList();
        }


        public List<AccountDAL> GetAccounts()
        {
            var accounts = _accountContext.Accounts.ToList();
            return accounts.Select(a => a.ToDAL()).ToList();
            //var test = _accountContext.Accounts.Include(a => a.TypeOfMoney).Include(a => a.TransactionsList).ToList();
            //return _accountContext.Accounts.ToList().Select(item => item.ToDAL()).ToList();
        }

        public List<CashFlowItemDAL> GetCashFlowView()
        {
            var result = new List<CashFlowItemDAL>();
            var currentDate = _accountContext.CrossCourses.OrderBy(c => c.CourseDate).First().CourseDate;
            var endDate = _accountContext.CrossCourses.OrderByDescending(c => c.CourseDate).First().CourseDate;
            decimal usdBalance = 0;
            decimal blrBalance = 0;
            var denomination = _accountContext.Denominations.Include(d => d.TypeOfMoney).Where(d => d.TypeOfMoney.Name == "Br").First().DifferenceToOne;
            while (currentDate <= endDate)
            {
                var transactionsList = _accountContext.Transactions.Where(t => t.CreateDate == currentDate).ToList();
                usdBalance += transactionsList.Where(t => t.Account.TypeOfMoney.Name == "Usd" && t.TypeOfTransaction.Category.Name == "Inc").ToList().Sum(t => t.Amount);
                usdBalance -= transactionsList.Where(t => t.Account.TypeOfMoney.Name == "Usd" && t.TypeOfTransaction.Category.Name == "Exp").ToList().Sum(t => t.Amount);
                blrBalance += transactionsList.Where(t => t.Account.TypeOfMoney.Name == "Br" && t.TypeOfTransaction.Category.Name == "Inc").ToList().Sum(t => t.Amount);
                blrBalance -= transactionsList.Where(t => t.Account.TypeOfMoney.Name == "Br" && t.TypeOfTransaction.Category.Name == "Exp").ToList().Sum(t => t.Amount);
                result.Add(new CashFlowItemDAL
                {
                    Date = currentDate,
                    BlrBalance = blrBalance,
                    OldBlrBalance = blrBalance * denomination,
                    UsdBalance = usdBalance
                });
                currentDate = currentDate.AddDays(1);
            }
            return result;
        }

        #region Contains of throw new NotImplementedException();

        public List<TransferInfrastructure.Models.CrossCourseDAL> GetCrossCourses()
        {
            throw new NotImplementedException();
        }

        public void SetCrossCourse(TransferInfrastructure.Models.CrossCourseDAL course)
        {
            throw new NotImplementedException();
        }

        public List<TransferInfrastructure.Models.DenominationDAL> GetDenomination()
        {
            throw new NotImplementedException();
        }

        public void SetDenomination(TransferInfrastructure.Models.DenominationDAL denomination)
        {
            throw new NotImplementedException();
        }

        public void SetTypeOfMoney(TransferInfrastructure.Models.TypeOfMoneyDAL typeOfMoney)
        {
            throw new NotImplementedException();
        }

        public List<TransferInfrastructure.Models.TransactionDAL> GetTransactions(int? accountId)
        {
            throw new NotImplementedException();
        }

        public void SetTransaction(TransferInfrastructure.Models.TransactionDAL transaction)
        {
            throw new NotImplementedException();
        }

        public List<TransferInfrastructure.Models.TypeOfTransactionDAL> GetTypesTransaction()
        {
            throw new NotImplementedException();
        }

        public void SetTypeOfTransaction(TransferInfrastructure.Models.TypeOfTransactionDAL transaction)
        {
            throw new NotImplementedException();
        }

        public List<TransferInfrastructure.Models.CategoryOfTransactionDAL> GetCategoriesOfTransaction()
        {
            throw new NotImplementedException();
        }

        public void SetCategoryOfTransaction(TransferInfrastructure.Models.CategoryOfTransactionDAL categoryOfTransaction)
        {
            throw new NotImplementedException();
        }

        public void SetAccount(TransferInfrastructure.Models.AccountDAL account)
        {
            throw new NotImplementedException();
        }
        #endregion

    }
}
