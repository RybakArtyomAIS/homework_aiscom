﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ORM.DAL.TransferInfrastructure.Models;

namespace ORM.DAL.EF.Entities
{
    public static class ExtensionsMethods
    {
        public static AccountDAL ToDAL(this AccountEF entity)
        {
            var result = new AccountDAL
            {
                Id = entity.Id,
                Amount = entity.Total,
                TypeOfMoney = entity.TypeOfMoney.ToDAL()
            };
            result.TransactionsList = entity.TransactionsList.ToList().Select(item => new TransactionDAL
            {
                Id = item.Id,
                Date = item.CreateDate,
                Total = item.Amount,
                Account = result,
                TypeOfTransaction = item.TypeOfTransaction.ToDAL()
            }).ToList();
            return result;
        }

        public static AccountEF ToEntity(this AccountDAL dal)
        {
            throw new Exception("Extension method DAL to EF doesn't make");
            //var result = new AccountEF
            //{
            //    Id = dal.Id,
            //    Amount = dal.Amount,
            //    TypeOfMoney = dal.TypeOfMoney.ToEntity()
            //};
            //result.TransactionList = dal.TransactionsList.Select(item => new TransactionEF
            //{
            //    Id = item.Id,
            //    Date = item.Date,
            //    Total = item.Total,
            //    AccountEF = result,
            //    TypeOfTransactionEF = item.TypeOfTransaction.ToEntity()
            //}).ToList();
            //return result;
        }

        public static TransactionDAL ToDAL(this TransactionEF entity)
        {
            return new TransactionDAL
            {
                Id = entity.Id,
                Date = entity.CreateDate,
                Total = entity.Amount,
                Account = entity.Account.ToDAL(),
                TypeOfTransaction = entity.TypeOfTransaction.ToDAL()
            };
        }

        public static TransactionEF ToEntity(this TransactionDAL dal)
        {
            throw new Exception("Extension method DAL to EF doesn't make");
            //return new TransactionEF
            //{
            //    Id = dal.Id,
            //    Date = dal.Date,
            //    Total = dal.Total, 
            //    AccountEF = dal.Account.ToEntity(),
            //    TypeOfTransactionEF = dal.TypeOfTransaction.ToEntity()
            //};
        }


        public static TypeOfTransactionDAL ToDAL(this TypeOfTransactionEF entity)
        {
            return new TypeOfTransactionDAL
            {
                Id = entity.Id,
                Name = entity.Name,
                CategoryOfTransaction = entity.Category.ToDAL()
            };
        }

        public static TypeOfTransactionEF ToEntity(this TypeOfTransactionDAL dal)
        {
            throw new Exception("Extension method DAL to EF doesn't make");
            //return new TypeOfTransactionEF
            //{
            //    Id = dal.Id,
            //    Name = dal.Name,
            //    CategoryOfTransactionEF = dal.CategoryOfTransaction.ToEntity()
            //};
        }


        public static CategoryOfTransactionDAL ToDAL(this CategoryOfTransactionEF entity)
        {
            return new CategoryOfTransactionDAL
            {
                Id = entity.Id,
                Name = entity.Name
            };
        }

        public static CategoryOfTransactionEF ToEntity(this CategoryOfTransactionDAL dal)
        {
            throw new Exception("Extension method DAL to EF doesn't make");
            //return new CategoryOfTransactionEF
            //{
            //    Id = dal.Id,
            //    Name = dal.Name
            //};
        }


        public static TypeOfMoneyDAL ToDAL(this TypeOfMoneyEF entity)
        {
            var result = new TypeOfMoneyDAL
            {
                Id = entity.Id,
                Name = entity.Name
            };
            result.CrossCourseList = entity.CrossCourses.Select(item => new CrossCourseDAL
            {
                Id = item.Id,
                Difference = item.Difference,
                Date = item.CourseDate,
                FirstTypeOfMoney = result, 
                SecondTypeOfMoneyId = item.SecondTypeId, 
                SecondTypeOfMoneyName = item.SecondTypeOfMoney.Name
            }).ToList();
            result.DenominationList = entity.Denominations.Select(item => new DenominationDAL
            {
                Id = item.Id,
                DifferenceToOne = item.DifferenceToOne,
                TypeOfMoney = result
            }).ToList();
            return result;
        }

        public static TypeOfMoneyEF ToEntity(this TypeOfMoneyDAL dal)
        {
            throw new Exception("Extension method DAL to EF doesn't make");
            //var result = new TypeOfMoneyEF
            //{
            //    Id = dal.Id,
            //    Name = dal.Name
            //};
            //result.CrossCourseList = dal.CrossCourseList.Select(item => new CrossCourseEF
            //{
            //    Id = item.Id,
            //    Difference = item.Difference,
            //    Date = item.Date,
            //    FirstTypeOfMoneyEF = result, 
            //    SecondTypeOfMoneyId = item.SecondTypeOfMoneyId, 
            //    SecondTypeOfMoneyName = item.SecondTypeOfMoneyName
            //}).ToList();
            //result.DenominationList = dal.DenominationList.Select(item => new DenominationEF
            //{
            //    Id = item.Id,
            //    DifferenceToOne = item.DifferenceToOne,
            //    TypeOfMoneyEF = result
            //}).ToList();
            //return result;
        }


        public static CrossCourseDAL ToDAL(this CrossCourseEF entity)
        {
            return new CrossCourseDAL
            {
                Id = entity.Id,
                Difference = entity.Difference,
                Date = entity.CourseDate,
                FirstTypeOfMoney = entity.FirstTypeOfMoney.ToDAL(), 
                SecondTypeOfMoneyId = entity.SecondTypeId, 
                SecondTypeOfMoneyName = entity.SecondTypeOfMoney.Name
            };
        }

        public static CrossCourseEF ToEntity(this CrossCourseDAL dal)
        {
            throw new Exception("Extension method DAL to EF doesn't make");
            //return new CrossCourseEF
            //{
            //    Id = dal.Id,
            //    Difference = dal.Difference,
            //    Date = dal.Date,
            //    FirstTypeOfMoneyEF = dal.FirstTypeOfMoney.ToEntity(), 
            //    SecondTypeOfMoneyId = dal.SecondTypeOfMoneyId, 
            //    SecondTypeOfMoneyName = dal.SecondTypeOfMoneyName
            //};
        }


        public static DenominationDAL ToDAL(this DenominationEF entity)
        {
            return new DenominationDAL
            {
                Id = entity.Id,
                DifferenceToOne = entity.DifferenceToOne,
                TypeOfMoney = entity.TypeOfMoney.ToDAL()
            };
        }

        public static DenominationEF ToEntity(this DenominationDAL dal)
        {
            throw new Exception("Extension method DAL to EF doesn't make");
            //return new DenominationEF
            //{
            //    Id = dal.Id,
            //    DifferenceToOne = dal.DifferenceToOne,
            //    TypeOfMoneyEF = dal.TypeOfMoney.ToEntity()
            //};
        }

        public static CashFlowItemDAL ToDAL(this CashFlowItemEF entity)
        {
            return new CashFlowItemDAL
            {
                Date = entity.Date,
                UsdBalance = entity.UsdBalance,
                BlrBalance = entity.BlrBalance,
                OldBlrBalance = entity.OldBlrBalance
            };
        }

        public static CashFlowItemEF ToEntity(this CashFlowItemDAL dal)
        {
            throw new Exception("Extension method DAL to EF doesn't make");
        //    return new CashFlowItemEF
        //    {
        //        Date = dal.Date,
        //        UsdBalance = dal.UsdBalance,
        //        BlrBalance = dal.BlrBalance,
        //        OldBlrBalance = dal.OldBlrBalance
        //    };
        }

    }
}
