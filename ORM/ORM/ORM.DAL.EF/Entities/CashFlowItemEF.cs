﻿using System;

namespace ORM.DAL.EF.Entities
{
    public class CashFlowItemEF
    {
        public DateTime Date { get; set; }

        public decimal UsdBalance { get; set; }

        public decimal BlrBalance { get; set; }

        public decimal OldBlrBalance { get; set; }
    }
}
