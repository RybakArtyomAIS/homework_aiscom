﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORM.DAL.EF.Entities
{
    public class AccountEF
    {
        public int Id { get; set; }

        public decimal Total { get; set; }

        public virtual TypeOfMoneyEF TypeOfMoney { get; set; }

        public int TypeOfMoneyId { get; set; }

        public virtual ICollection<TransactionEF> TransactionsList { get; set; }
    }
}
