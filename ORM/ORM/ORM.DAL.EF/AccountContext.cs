﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ORM.DAL.EF.FluentAPIConfigs;
using ORM.DAL.EF.Entities;

namespace ORM.DAL.EF
{
    public class AccountContext: DbContext
    {
        public AccountContext()
            : base("Name=AccountDb")
        {
            //this.Configuration.LazyLoadingEnabled = false;
        }

        public DbSet<AccountEF> Accounts { get; set; }

        public DbSet<TransactionEF> Transactions { get; set; }

        public DbSet<TypeOfTransactionEF> TypesOfTransaction { get; set; }

        public DbSet<CategoryOfTransactionEF> CategoriesOfTransaction { get; set; }

        public DbSet<TypeOfMoneyEF> TypesOfMoney { get; set; }

        public DbSet<DenominationEF> Denominations { get; set; }

        public DbSet<CrossCourseEF> CrossCourses { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new AccountConfig());

            modelBuilder.Configurations.Add(new TransactionConfig());

            modelBuilder.Configurations.Add(new TypeOfMoneyConfig());

            modelBuilder.Configurations.Add(new TypeOfTransactionConfig());

            modelBuilder.Configurations.Add(new CategoryConfig());

            modelBuilder.Configurations.Add(new DenominationConfig());

            modelBuilder.Configurations.Add(new CrossCourseConfig());
            //base.OnModelCreating(modelBuilder);
        }
    }
}
