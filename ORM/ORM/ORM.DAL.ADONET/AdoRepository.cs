﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Common;
using ORM.DAL.TransferInfrastructure;
using ORM.DAL.TransferInfrastructure.Models;

namespace ORM.DAL.ADONET
{
    public class AdoRepository : IRepository
    {
        private readonly string _dataProvider;
        private readonly string _connectionString;

        public AdoRepository()
        {
            _dataProvider = ConfigurationManager.AppSettings["provider"];
            _connectionString = ConfigurationManager.AppSettings["cnStr"];
        }


        public List<CashFlowItemDAL> GetCashFlowView()
        {
            var result = new List<CashFlowItemDAL>();
            var df = DbProviderFactories.GetFactory(_dataProvider);

            using (var cn = df.CreateConnection())
            {
                cn.ConnectionString = _connectionString;
                cn.Open();

                var cmd = df.CreateCommand();
                cmd.Connection = cn;
                cmd.CommandText = "EXEC ExecuteCashFlowView";
                using (var dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        result.Add(new CashFlowItemDAL
                        {
                            BlrBalance = (Decimal)dr["BlrBalance"],
                            Date = (DateTime)dr["Date"],
                            OldBlrBalance = (Decimal)dr["OldBlrBalance"],
                            UsdBalance = (Decimal)dr["UsdBalance"]
                        });
                    }
                }
            }

            return result;
        }

        public List<CrossCourseDAL> GetCrossCourses()
        {
            var result = new List<CrossCourseDAL>();
            var df = DbProviderFactories.GetFactory(_dataProvider);

            using (var cn = df.CreateConnection())
            {
                cn.ConnectionString = _connectionString;
                cn.Open();

                var cmd = df.CreateCommand();
                cmd.Connection = cn;
                cmd.CommandText = "EXEC GetCrossCourses " + 0;
                using (var dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        result.Add(new CrossCourseDAL
                        {
                            Id = (int)dr["Id"],
                            Date = (DateTime)dr["Date"],
                            Difference = (double)dr["Difference"],
                            SecondTypeOfMoneyId = (int)dr["SecondId"],
                            SecondTypeOfMoneyName = dr["SecondName"].ToString()
                        });
                    }
                }
            }

            return result;
        }

        public List<TransactionDAL> GetTransactions(int? accountId)
        {
            var result = new List<TransactionDAL>();
            var df = DbProviderFactories.GetFactory(_dataProvider);

            using (var cn = df.CreateConnection())
            {
                cn.ConnectionString = _connectionString;
                cn.Open();

                var cmd = df.CreateCommand();
                cmd.Connection = cn;
                cmd.CommandText = "EXEC GetAccountTransactions " + Convert.ToString(accountId ?? -1);
                using (var dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        result.Add(new TransactionDAL
                        {
                            Id = (int)dr["Id"],
                            Date = (DateTime)dr["CreateDate"],
                            Total = (Decimal)dr["Amount"],
                            TypeOfTransaction = new TypeOfTransactionDAL
                            {
                                Id = (int)dr["TypeID"],
                                Name = dr["TypeName"].ToString(),
                                CategoryOfTransaction = new CategoryOfTransactionDAL
                                {
                                    Id = (int)dr["CategoryID"],
                                    Name = dr["CategoryName"].ToString()
                                }
                            }
                        });
                    }
                }
            }

            return result;
        }

        public List<AccountDAL> GetAccounts()
        {
            var result = new List<AccountDAL>();
            var df = DbProviderFactories.GetFactory(_dataProvider);

            using (var cn = df.CreateConnection())
            {
                cn.ConnectionString = _connectionString;
                cn.Open();

                var cmd = df.CreateCommand();
                cmd.Connection = cn;
                cmd.CommandText = "EXEC GetAccounts";
                using (var dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        result.Add(new AccountDAL
                        {
                            Id = (int)dr["Id"],
                            Amount = (decimal)dr["Total"],
                            TypeOfMoney = new TypeOfMoneyDAL
                            {
                                Id = (int)dr["TypeOfMoneyId"],
                                Name = dr["TypeOfMoneyName"].ToString(),
                                CrossCourseList = new List<CrossCourseDAL>(),
                                DenominationList = new List<DenominationDAL>()
                            }
                        });
                    }
                }

                foreach (var account in result)
                {
                    var transactionList = new List<TransactionDAL>();
                    cmd = df.CreateCommand();
                    cmd.Connection = cn;
                    cmd.CommandText = "EXEC GetAccountTransactions " + account.Id;
                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            transactionList.Add(new TransactionDAL
                            {
                                Id = (int)dr["Id"],
                                Date = (DateTime)dr["CreateDate"],
                                Total = (Decimal)dr["Amount"],
                                TypeOfTransaction = new TypeOfTransactionDAL
                                {
                                    Id = (int)dr["TypeID"],
                                    Name = dr["TypeName"].ToString(),
                                    CategoryOfTransaction = new CategoryOfTransactionDAL
                                    {
                                        Id = (int)dr["CategoryID"],
                                        Name = dr["CategoryName"].ToString()
                                    }
                                },
                                Account = account
                            });
                        }
                    }
                    account.TransactionsList = transactionList;
                }
            }

            return result;
        }

        public List<TransferInfrastructure.Models.TypeOfMoneyDAL> GetTypesOfMoney()
        {
            var result = new List<TypeOfMoneyDAL>();
            var df = DbProviderFactories.GetFactory(_dataProvider);

            using (var cn = df.CreateConnection())
            {
                cn.ConnectionString = _connectionString;
                cn.Open();

                var cmd = df.CreateCommand();
                cmd.Connection = cn;
                cmd.CommandText = "EXEC GetTypesOfMoney";
                using (var dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        result.Add(new TypeOfMoneyDAL
                        {
                            Id = (int)dr["Id"],
                            Name = dr["Name"].ToString()
                        });
                    }
                }

                foreach (var typeOfMoney in result)
                {
                    var crossCourseList = new List<CrossCourseDAL>();
                    var denominationList = new List<DenominationDAL>();
                    cmd = df.CreateCommand();
                    cmd.Connection = cn;
                    cmd.CommandText = "EXEC GetCrossCourses " + typeOfMoney.Id;
                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            crossCourseList.Add(new CrossCourseDAL
                            {
                                Id = (int)dr["Id"],
                                Date = (DateTime)dr["Date"],
                                Difference = (double)dr["Difference"], 
                                SecondTypeOfMoneyId = (int)dr["SecondId"], 
                                SecondTypeOfMoneyName = dr["SecondName"].ToString()
                            });
                        }
                    }
                    typeOfMoney.CrossCourseList = crossCourseList;

                    cmd = df.CreateCommand();
                    cmd.Connection = cn;
                    cmd.CommandText = "EXEC GetDenomination " + typeOfMoney.Id;
                    using (var dr = cmd.ExecuteReader())
                    {
                        if (dr.HasRows)
                            while (dr.Read())
                            {
                                denominationList.Add(new DenominationDAL
                                {
                                    Id = (int)dr["Id"],
                                    DifferenceToOne = (decimal)dr["DifferenceToOne"],
                                    TypeOfMoney = typeOfMoney
                                });
                            }
                    }
                    typeOfMoney.DenominationList = denominationList;
                }
            }
            return result;
        }

        #region Contains of throw new System.NotImplementedException();

        public void SetCrossCourse(TransferInfrastructure.Models.CrossCourseDAL course)
        {
            throw new NotImplementedException();
        }

        public List<TransferInfrastructure.Models.DenominationDAL> GetDenomination()
        {
            throw new NotImplementedException();
        }

        public void SetDenomination(TransferInfrastructure.Models.DenominationDAL denomination)
        {
            throw new NotImplementedException();
        }

        public void SetTypeOfMoney(TransferInfrastructure.Models.TypeOfMoneyDAL typeOfMoney)
        {
            throw new NotImplementedException();
        }

        public void SetTransaction(TransferInfrastructure.Models.TransactionDAL transaction)
        {
            throw new NotImplementedException();
        }

        public List<TransferInfrastructure.Models.TypeOfTransactionDAL> GetTypesTransaction()
        {
            throw new NotImplementedException();
        }

        public void SetTypeOfTransaction(TransferInfrastructure.Models.TypeOfTransactionDAL transaction)
        {
            throw new NotImplementedException();
        }

        public List<TransferInfrastructure.Models.CategoryOfTransactionDAL> GetCategoriesOfTransaction()
        {
            throw new NotImplementedException();
        }

        public void SetCategoryOfTransaction(TransferInfrastructure.Models.CategoryOfTransactionDAL categoryOfTransaction)
        {
            throw new NotImplementedException();
        }

        public void SetAccount(TransferInfrastructure.Models.AccountDAL account)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
