﻿using System.Collections.Generic;
using ORM.DAL.TransferInfrastructure.Models;

namespace ORM.DAL.TransferInfrastructure
{
    public interface IRepository
    {
        List<CrossCourseDAL> GetCrossCourses();
        void SetCrossCourse(CrossCourseDAL course);

        List<DenominationDAL> GetDenomination();
        void SetDenomination(DenominationDAL denomination);

        List<TypeOfMoneyDAL> GetTypesOfMoney();
        void SetTypeOfMoney(TypeOfMoneyDAL typeOfMoney);
        
        List<TransactionDAL> GetTransactions(int? accountId);
        void SetTransaction(TransactionDAL transaction);

        List<TypeOfTransactionDAL> GetTypesTransaction();
        void SetTypeOfTransaction(TypeOfTransactionDAL transaction);

        List<CategoryOfTransactionDAL> GetCategoriesOfTransaction();
        void SetCategoryOfTransaction(CategoryOfTransactionDAL categoryOfTransaction);

        List<AccountDAL> GetAccounts();
        void SetAccount(AccountDAL account);

        List<CashFlowItemDAL> GetCashFlowView();
    }
}
