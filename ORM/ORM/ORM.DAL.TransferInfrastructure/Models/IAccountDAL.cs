﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORM.DAL.TransferInfrastructure.Models
{
    public interface IAccountDAL
    {
        int Id { get; set; }
        decimal Amount { get; set; }
        ITypeOfMoneyDAL TypeOfMoney { get; set; }
        List<ITransactionDAL> TransactionsList { get; set; }
    }

    public class AccountDAL
    {
        public int Id { get; set; }

        public decimal Amount { get; set; }

        public TypeOfMoneyDAL TypeOfMoney { get; set; }

        public List<TransactionDAL> TransactionsList { get; set; }
    }
}
