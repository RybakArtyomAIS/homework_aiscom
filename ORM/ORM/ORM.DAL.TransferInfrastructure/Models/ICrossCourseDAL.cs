﻿using System;
namespace ORM.DAL.TransferInfrastructure.Models
{
    public interface ICrossCourseDAL
    {
        int Id { get; set; }
        double Difference { get; set; }
        ITypeOfMoneyDAL FirstTypeOfMoney { get; set; }
        ITypeOfMoneyDAL SecondTypeOfMoney { get; set; }
    }

    public class CrossCourseDAL
    {
        public int Id { get; set; }

        public double Difference { get; set; }

        public DateTime Date { get; set; }

        public TypeOfMoneyDAL FirstTypeOfMoney { get; set; }

        public int SecondTypeOfMoneyId { get; set; }

        public string SecondTypeOfMoneyName { get; set; }
    }
}