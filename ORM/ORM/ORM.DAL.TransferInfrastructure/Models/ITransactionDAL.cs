﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORM.DAL.TransferInfrastructure.Models
{
    public interface ITransactionDAL
    {
        int Id { get; set; }
        DateTime Date { get; set; }
        decimal Total { get; set; }
        ITypeOfTransactionDAL TypeOfTransaction { get; set; }
        IAccountDAL Account { get; set; }
    }

    public class TransactionDAL
    {
        public int Id { get; set; }

        public DateTime Date { get; set; }

        public decimal Total { get; set; }

        public TypeOfTransactionDAL TypeOfTransaction { get; set; }

        public AccountDAL Account { get; set; }
    }
}
