﻿using System;

namespace ORM.DAL.TransferInfrastructure.Models
{
    public interface ICashFlowItemDAL
    {
        DateTime Date { get; set; }
        decimal UsdBalance { get; set; }
        decimal BlrBalance { get; set; }
        decimal OldBlrBalance { get; set; }
    }

    public class CashFlowItemDAL
    {
        public DateTime Date { get; set; }

        public decimal UsdBalance { get; set; }

        public decimal BlrBalance { get; set; }

        public decimal OldBlrBalance { get; set; }
    }
}
