﻿namespace ORM.DAL.TransferInfrastructure.Models
{
    public interface IDenominationDAL
    {
        int Id { get; set; }
        double DifferenceToOne { get; set; }
        ITypeOfMoneyDAL TypeOfMoney { get; set; }
    }

    public class DenominationDAL
    {
        public int Id { get; set; }

        public decimal DifferenceToOne { get; set; }

        public TypeOfMoneyDAL TypeOfMoney { get; set; }
    }
}