﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORM.DAL.TransferInfrastructure.Models
{
    public interface ITypeOfMoneyDAL
    {
        int Id { get; set; }
        string Name { get; set; }
        List<IDenominationDAL> DenominationList { get; set; }
        List<ICrossCourseDAL> CrossCourseList { get; set; }
    }

    public class TypeOfMoneyDAL
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public List<DenominationDAL> DenominationList { get; set; }

        public List<CrossCourseDAL> CrossCourseList { get; set; }
    }
}
