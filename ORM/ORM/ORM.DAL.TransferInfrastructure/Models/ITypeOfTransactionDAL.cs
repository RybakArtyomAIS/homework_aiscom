﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORM.DAL.TransferInfrastructure.Models
{
    public interface ITypeOfTransactionDAL
    {
        int Id { get; set; }
        string Name { get; set; }
        ICategoryOfTransactionDAL CategoryOfTransaction { get; set; }
    }

    public class TypeOfTransactionDAL
    {

        public int Id { get; set; }

        public string Name { get; set; }

        public CategoryOfTransactionDAL CategoryOfTransaction { get; set; }
    }
}
