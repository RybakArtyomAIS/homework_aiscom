﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORM.DAL.TransferInfrastructure.Models
{
    public interface ICategoryOfTransactionDAL
    {
        int Id { get; set; }
        string Name { get; set; }
    }

    public class CategoryOfTransactionDAL
    {

        public int Id { get; set; }

        public string Name { get; set; }
    }
}
