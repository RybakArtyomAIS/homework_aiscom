﻿var yandexTranslateAPIKey = "trnsl.1.1.20170102T104548Z.36f122573b6166e5.efd0e597d2207912f6c0b19411038a42786bb0b9";
var engRuTranslate = "en-ru";
var ruEngTranslate = "ru-en";
var yandexTranslateUrl = "https://translate.yandex.net/api/v1.5/tr.json/translate";
var mainModule = angular.module('main', []);

mainModule.controller("homePageViewModel", function($scope, $http, $location) {
    $scope.russianText = "This is result";
    $scope.executeTranslate = function() {
        var req = {
            method: 'POST',
            url: yandexTranslateUrl + "?lang=" + engRuTranslate + "&key=" + yandexTranslateAPIKey,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Accept': '*/*'
            },
            text: $scope.englishText
                //data: {
                //text: $scope.englishText,
                //key: yandexTranslateAPIKey,
                //lang: engRuTranslate
                //}
        };
        $http(req).then(function(obj) {
            $scope.russianText = obj.data.text[0];
        }, function(obj) {
            alert("Code - " + obj.data.code + "; Message - " + obj.data.message);
        })
    }
});